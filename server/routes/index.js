module.exports = function (app) {    
    const indexController = require('../controllers');
    const landingController = require('../controllers/landings');
    const imgController = require('../controllers/images');
    const mid = require('../middleware');

    app.route('/api').get(landingController.welcome)    
    app.route('/api/login').post(mid.auth, indexController.login)    
    app.route('/api/landing').get(landingController.getLanding)    
    app.route('/api/ucc-menu').get(landingController.getLandingMenu)    
    app.route('/api/images').post(imgController.imageUploader)
    app.route('/api/get-key').get(indexController.getKey)
    app.route('/api/ucc-api').post(mid.checkBody, mid.auth, indexController.uccApi)
    app.route('/api/ucc-api-private').post(mid.checkBody, mid.authUser, indexController.uccApi)
    app.route('/api/ucc-api-public').post(mid.checkBody, mid.nonAuth, indexController.uccApiPublic)
    app.route('/api/check-payload').post(mid.checkBody, mid.checkPayload)

    app.route('/api/ucc-images')
    .get(indexController.getImages)
    .post(mid.checkBodyImage, mid.auth, imgController.imageUploader, indexController.setImages)

    app.route('/api/landing-profile')
    .get(mid.auth, landingController.getProfile)
    .put(mid.auth, landingController.putProfile)
    app.route('/api/landing-slider')
    .get(mid.auth, landingController.getSlider)
    .post(mid.auth, landingController.postSlider)
    .put(mid.auth, landingController.putSlider)
    app.route('/api/landing-menu')
    .get(mid.auth, landingController.getMenu)
    .post(mid.auth, landingController.postMenu)
    .put(mid.auth, landingController.putMenu)
    app.route('/api/landing-feature')
    .get(mid.auth, landingController.getFeature)
    .put(mid.auth, landingController.putFeature)
    app.route('/api/landing-about')
    .get(mid.auth, landingController.getAbout)
    .put(mid.auth, landingController.putAbout)
    app.route('/api/page-content/:id')
    .get(mid.nonAuth, landingController.pageContent)

    app.route('/api/user-menu').post(mid.checkBody, mid.authUser, indexController.userMenu)


};
