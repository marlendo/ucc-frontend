require('dotenv').config()
const axios = require("axios");
const response = require('../helper');
const AES = require('../helper/AES');
const ENV = process.env.NODE_ENV

let url = 'http://localhost:3001/';
if (ENV === 'local') {
    url = 'http://157.230.42.171:3001/';
}


const header = {
    "Content-Type": "application/json",
    "Accept": "application/json"
}

let uccPayload = {
    "header": {
        "requestId": "isiusername+datetime",
        "requestTime": "waktu melakukan request",
        "activty": "aktifitas yg dilakukan, ex skrng login",
        "username": "username",
        "access": "ambil balikan param seteleh login, klw blm login/kosong param dr backend isi null aj",
        "device": "ex browser mozila, ex samsung galaxy",
        "deviceVersion": "version device",
        "token": null,
        "jwtKey": null,
        "location": "lat lang"
    },
    "body": {

    }
}

async function request({
    method, url, headers, data
}) {
    const payload = {
        headers,
        method,
        url,
        data
    }

    const res = await axios(payload);
    return res.data;
}

exports.checkPayload = async function (req, res, next) {
    try {
        const { authorization = '' } = req.headers
        const [type, token] = authorization.split(" ")
        if (type === 'adminKey' && token.length > 10) {
            const data = await request({
                method: 'get',
                url: url + 'getKey',
                headers: null,
                data: null
            })
            if (data.status.toString() !== "200") {
                response.failed(res, data)
            } else {
                uccPayload.header.jwtKey = data.values;
                uccPayload.body = AES.decripter(token, data.values);
                const payload = {
                    headers: header,
                    method: req.body.method,
                    url: url + req.body.url,
                    body: req.body.data,
                    data: uccPayload
                };
                response.success(res, payload)
            }
        } else {
            let message = "Unauthorized"
            if (authorization === "") {
                message = "`Authorization` header is required"
            } else if (token.length < 10) {
                message = "Invalid API key"
            }
            response.failed(res, message)
        }
    } catch (err) {
        console.log(err)
        response.failed(res, err)
    }
};

exports.auth = async function (req, res, next) {
    try {
        const { authorization = '' } = req.headers
        const [type, token] = authorization.split(" ")
        if (type === 'adminKey' && token.length > 10) {
            const data = await request({
                method: 'get',
                url: url + 'getKey',
                headers: null,
                data: null
            })
            if (data.status.toString() !== "200") {
                response.failed(res, data)
            } else {
                uccPayload.header.jwtKey = data.values;
                uccPayload.body = AES.decripter(token, data.values);
                const doAuth = await request({
                    method: 'post',
                    url: url + 'loginAdmin',
                    headers: header,
                    data: uccPayload
                })
                if (doAuth.status.toString() !== "200") {
                    response.failed(res, doAuth)
                } else {
                    uccPayload.header.token = doAuth.values;
                    req.token = {
                        token,
                        key: data.values,
                        jwt: doAuth.values
                    }
                    req.uccPayload = uccPayload;
                    next();
                }
            }
        } else {
            let message = "Unauthorized"
            if (authorization === "") {
                message = "`Authorization` header is required"
            } else if (token.length < 10) {
                message = "Invalid API key"
            }
            response.failed(res, message)
        }
    } catch (err) {
        console.log(err)
        response.failed(res, err)
    }
};

exports.authUser = async function (req, res, next) {
    try {
        const { authorization = '' } = req.headers
        const [type, token] = authorization.split(" ")
        if (type === 'adminKey' && token.length > 10) {
            const data = await request({
                method: 'get',
                url: url + 'getKey',
                headers: null,
                data: null
            })
            if (data.status.toString() !== "200") {
                response.failed(res, data)
            } else {
                uccPayload.header.jwtKey = data.values;
                uccPayload.body = AES.decripter(token, data.values);
                const doAuth = await request({
                    method: 'post',
                    url: url + 'loginEnduser',
                    headers: header,
                    data: uccPayload
                })
                if (doAuth.status.toString() !== "200") {
                    response.failed(res, doAuth)
                } else {
                    uccPayload.header.token = doAuth.values;
                    req.token = {
                        token,
                        key: data.values,
                        jwt: doAuth.values
                    }
                    req.uccPayload = uccPayload;
                    next();
                }
            }
        } else {
            let message = "Unauthorized"
            if (authorization === "") {
                message = "`Authorization` header is required"
            } else if (token.length < 10) {
                message = "Invalid API key"
            }
            response.failed(res, message)
        }
    } catch (err) {
        console.log(err)
        response.failed(res, err)
    }
};

exports.nonAuth = async function (req, res, next) {
    try {
        req.uccPayload = uccPayload;
        next();
    } catch (err) {
        console.log(err)
        response.failed(res, err)
    }
};

exports.checkBody = async function (req, res, next) {
    try {
        if (req.body.url == undefined) {
            response.failed(res, 'url not found')
        } else if (req.body.method == undefined) {
            response.failed(res, 'method not found')
        } else {
            next()
        }
    } catch (err) {
        response.failed(res, err)
    }
};

exports.checkBodyImage = async function (req, res, next) {
    try {
        const imageType = req.headers.imagetype
        console.log(imageType);
        if (imageType == undefined) {
            response.failed(res, 'type image not found')
        } else {
            req.imageType = imageType
            next()
        }
    } catch (err) {
        response.failed(res, err)
    }
};

exports.getMenu = async function (req, res, next) {
    try {
        const data = await request({
            method: 'post',
            url: url + 'masterAdminAccess',
            headers: header,
            data: uccPayload
        })
        if (data.status.toString() !== "200") {
            response.failed(res, uccPayload)
        } else {
            req.uccMenu = data.values
            next();
        }
    } catch (err) {
        console.log(err)
        response.failed(res, err)
    }
};