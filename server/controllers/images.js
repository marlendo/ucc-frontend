const response = require('../helper');
const ENV = process.env.NODE_ENV;
const port = process.env.PORT;
const con = require('../db');
const multer = require('multer');
const sharp = require('sharp');
const fs = require('fs');

exports.imageUploader = (req, res, next) => {

  const storage = multer.diskStorage({
    destination: (req, file, callback) => {
      callback(null, (__dirname + './../../images/'));
    },
    filename: (req, file, callback) => {
      callback(null, Date.now() + '-' + file.originalname);
    }
  });

  const upload = multer({ storage: storage }).any('file');

  try {

    upload(req, res, (err) => {
      if (err) {
        return res.status(400).send({
          error: true,
          message: 'error upload',
          data: err
        });
      } else {
        let results = req.files.map((file) => {

          return {
            mediaName: file.filename,
            origMediaName: file.originalname,
            mediaSource: file.filename
          }
        });

        const path = __dirname + './../../images/';
        const filePath = path + results[0].mediaName;

        //   sharp(filePath)
        //     .resize({ width: 100 })
        //     .toBuffer()
        //     .then(data => {
        //       fs.writeFileSync(filePath, data);
        //     })
        //     .catch(err => {
        //         console.log(err)
        //     });


        let data = {
          name: results[0].origMediaName,
          source: filePath,
          url: ENV === 'local' ? 'http://localhost:' + port + '/resources/' + results[0].mediaName : 'http://ucc-edu.com' + '/resources/' + results[0].mediaName
        }

        req.payload = data;
        next()
      }
    });

  } catch (err) {
    response.failed(res, err)
  }
}