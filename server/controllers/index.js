require('dotenv').config()
const axios = require('axios')
const response = require('../helper');
const con = require('../db');
const ENV = process.env.NODE_ENV;

let url = 'http://localhost:3001/';

if (ENV == 'local') {
    url = 'http://157.230.42.171:3001/'
}

async function request({
    method, url, headers, data
}) {
    const payload = {
        headers,
        method,
        url,
        data
    }

    const res = await axios(payload);
    return res.data;
}

exports.getKey = async function (req, res) {
    try {
        const data = await request({
            method: 'get',
            url: url + 'getKey',
            headers: null,
            data: null
        })
        if (data.status.toString() !== "200") {
            response.failed(res, data)
        } else {
            response.success(res, data)
        }
    } catch (err) {
        response.failed(res, err)
    }
};

exports.login = async function (req, res) {
    try {
        const data = req.token
        response.success(res, data)
    } catch (err) {
        response.failed(res, err)
    }
};

exports.uccApi = async function (req, res) {
    try {
        let payload = req.uccPayload;
        payload.body = req.body.data
        const data = await request({
            method: req.body.method,
            url: url + req.body.url,
            headers: null,
            data: payload
        })
        response.success(res, data)
    } catch (err) {
        response.failed(res, err)
    }
};

exports.uccApiPublic = async function (req, res) {
    try {
        let payload = req.uccPayload;
        payload.body = req.body.data
        const data = await request({
            method: req.body.method,
            url: url + req.body.url,
            headers: null,
            data: payload
        })
        response.success(res, data)
    } catch (err) {
        response.failed(res, err)
    }
};

exports.getImages = async function (req, res) {
    try {
        const data = await con.conLanding("select * from ucc_image");
        response.success(res, data)
    } catch (err) {
        response.failed(res, err)
    }
};

exports.setImages = async function (req, res) {
    try {
        const name = req.payload.name;
        const url = req.payload.url;
        const imageType = req.imageType;
        const sql = "INSERT INTO `ucc_image` (`id`, `type`, `url`, `name`) VALUES (NULL, '" +
            imageType + "', '" + url + "', '" + name + "')"
        const data = await con.conLanding(sql);
        response.success(res, data)
    } catch (err) {
        response.failed(res, err)
    }
};

exports.userMenu = async function (req, res) {
    try {
        let payload = req.uccPayload;
        payload.body = req.body.data
        const data = await request({
            method: req.body.method,
            url: url + req.body.url,
            headers: null,
            data: payload
        })
        let newData = [];
        if (Array.isArray(data.values)) {
            for (let i in data.values) {
                newData.push({
                    id: data.values[i].idsubmenu,
                    img: null,
                    title: data.values[i].submenuName,
                    child: []
                })
            }
        }
        response.success(res, newData)
    } catch (err) {
        response.failed(res, err)
    }
};