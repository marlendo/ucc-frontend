const CryptoJS = require("crypto-js");
const env = process.env.NODE_ENV;

exports.encripter = function (object, key) {
    try {
        let ciphertext = CryptoJS.AES.encrypt(JSON.stringify(object), key);        
        return ciphertext.toString();
    } catch (error) {
        console.log(error)
        return 'password encription error'
    }
}

exports.decripter = function (string, key) {
    try {
        var bytes = CryptoJS.AES.decrypt(string, key);        
        var plaintext = bytes.toString(CryptoJS.enc.Utf8);        
        return JSON.parse(plaintext);
    } catch (error) {
        return 'password encription error'
    }
}
