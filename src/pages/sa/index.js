import React, { useEffect, useState } from "react";
import { SuperAdminPage } from '../../components/view/superAdmin';
import { LoginPage } from "../../components/form";
import { getStorage } from "../../helpers/store";
import { TextTitle } from "../../components/text";

const Index = () => {

  const [login, setLogin] = useState(false);

  function getLogin() {
    const login = getStorage('loginsa');
    if (login !== null) {
      if (login.token !== undefined) {
        setLogin(true)
      }
    }
  }

  useEffect(() => {
    getLogin()
  }, [])

  return (
    <>
      {!login ? (
        <LoginPage type={'superAdmin'} />
      ) : (
          <SuperAdminPage />
        )}
    </>
  )
}

export default Index
