import React, { useEffect, useState } from "react";
import Layout from "../components/layout";
import SEO from "../components/seo";
import { Space, PageContent, StatisticContent, UCCMenu, GetAppContent, AboutCompoment, FeatureComponent } from "../components/container";
import { useTracked } from '../service';
import { Slider, Screenshot, Sponsore } from "../components/animation";
import { menuName } from "../constants";
import { StatisticCard } from "../components/card";

import AccountBalanceIcon from '@material-ui/icons/AccountBalance';
import CloudDownloadIcon from '@material-ui/icons/CloudDownload';
import PeopleAltIcon from '@material-ui/icons/PeopleAlt';
import EmojiPeopleIcon from '@material-ui/icons/EmojiPeople';
import { FormContact } from "../components/form";
import { ImageBackground } from "../components/image";

import { getLanding } from '../service/api';

const IndexPage = () => {

  const [state, action] = useTracked();
  const [content, setContent] = useState(false)

  async function getData() {
    const data = await getLanding()
    setContent(data)
  }

  useEffect(() => {
    getData()
    window.addEventListener('scroll', handleScroll)
  }, [])

  useEffect(() => {
    return () => {
      window.removeEventListener('scroll', handleScroll)
    }
  }, [])

  function getElement(name) {
    const el = window.document.getElementById(name);
    if (el !== null) {
      return el.getBoundingClientRect().bottom
    } else {
      return false
    }
  }

  function getMenu(homeEl, featureEl, downloadEl, contactEl, sponsoreEl) {
    const height = window.innerHeight;
    if (homeEl <= height && homeEl < 110 && homeEl > 0) {
      changeMenu(menuName.home)
    } else if (featureEl <= height && featureEl < 110 && featureEl > 0) {
      changeMenu(menuName.features)
    }
    else if (downloadEl <= height && downloadEl < 110 && downloadEl > 0) {
      changeMenu(menuName.download)
    }
    else if (contactEl <= height && contactEl < 110 && contactEl > 0) {
      changeMenu(menuName.contact)
    }
    else if (sponsoreEl <= height && sponsoreEl < 110 && sponsoreEl > 0) {
      changeMenu(menuName.sponsore)
    }
  }

  function changeMenu(id) {
    action({ type: 'setMenu', menu: id });
  }

  const handleScroll = (event) => {
    const homeEl = getElement(menuName.home);
    const featureEl = getElement(menuName.features);
    const download = getElement(menuName.download);
    const contact = getElement(menuName.contact);
    const sponsore = getElement(menuName.sponsore);
    getMenu(homeEl, featureEl, download, contact, sponsore)
  }

  return (
    <Layout>
      <div id={menuName.home} />
      <SEO title="Home" />
      <ImageBackground />
      {content ? (
        <>
          <Slider payload={content.uccSlider} />
          <UCCMenu payload={content.uccMenu} />
        </>
      ) : (
          null
        )}
      <PageContent title={'Tentang UCC Apps'}>
        <AboutCompoment payload={content.uccAbout} />
      </PageContent>
      <PageContent id={menuName.features} title={'features'}>
        <FeatureComponent payload={content.uccFeatures} />
        <Space size={30} />
      </PageContent>
      <StatisticContent>
        <StatisticCard
          icon={
            <PeopleAltIcon className={'state-icon'} />
          }
          title={'20K ++'}
          content={'Total Mahasiswa UCC'}
        />
        <StatisticCard
          icon={
            <CloudDownloadIcon className={'state-icon'} />
          }
          title={'1M ++'}
          content={'Telah Downloaded'}
        />
        <StatisticCard
          icon={
            <EmojiPeopleIcon className={'state-icon'} />
          }
          title={'10K ++'}
          content={'Dosen Berkontribusi'}
        />
        <StatisticCard
          icon={
            <AccountBalanceIcon className={'state-icon'} />
          }
          title={'50K ++'}
          content={'Universitas Terdaftar'}
        />
      </StatisticContent>
      <PageContent id={menuName.download} title={'download'} padding={false} transparent>
        <GetAppContent />
        <Screenshot />
      </PageContent>
      <PageContent id={menuName.contact} title={'hubungi kami'}>
        {
          content ? (
            <FormContact payload={content.uccProfile} />
          ) : (
              null
            )
        }
        <Space size={50} />
      </PageContent>
      <PageContent id={menuName.sponsore} title={'supported by'}>
        <Sponsore />
      </PageContent>
    </Layout>
  )
}

export default IndexPage
