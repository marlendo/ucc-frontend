import React, { useEffect } from "react";
import Layout from "../../components/layout";
import SEO from "../../components/seo";
import colors from "../../constants/color";
import { Space, HeaderComponent, ContentComponent } from "../../components/container";
import { uccApiPublic, getLandingsContent } from "../../service/api";
import { menuRegistered, initialSubPage } from "../../constants";
import { useState } from "react";
import { TextContent, TextTitle } from "../../components/text";
import { useTracked } from '../../service';
import { PageHeader, PageHeaderUccSiap } from "../../components/header";
import { LoginPage, LoginRegisterPage } from "../../components/form";
import { validURL } from "../../helpers/validator";
import { getStorage } from "../../helpers/store";

const PageContent = (props) => {

  const [state, action] = useTracked();

  useEffect(() => {
    // getHeader()
  }, [])

  return (
    <div>
      <Space size={52} />
      {state.pageMenu.idlandingMenu === 1 ? (
        <div>
          <ContentComponent id={props.payload.idmasterAdminAccess} />
        </div>
      ) :
        state.pageMenu.idlandingMenu >= 3 ? (
          <div>
            <LoginRegisterPage payload={props.payload} />
          </div>
        ) : (
            <div />
          )
      }
    </div>
  )
}

const UCCPage = () => {

  const [state, action] = useTracked();
  const [menu, setMenu] = useState([]);
  const [seo, setSeo] = useState(menuRegistered[8])

  async function getHeader() {
    // const data = await uccApiPublic('inquirylandingPageMenuByIdAdminAccs', seo.idmasterAdminAccess) 
    const data = initialSubPage
    if (Array.isArray(data)) {
      action({
        type: 'setPageMenu',
        data: data[0]
      })
      setMenu(data)
    }
  }

  useEffect(() => {
    console.log(state)
    action({
      type: 'setMainPage',
      data: seo
    })
    getHeader()
  }, [])

  return (
    <Layout>
      <SEO title={seo.page} description={seo.desc} />
      <PageHeaderUccSiap payload={seo} />
      <PageContent payload={seo} />
    </Layout>
  )
}

export default UCCPage
