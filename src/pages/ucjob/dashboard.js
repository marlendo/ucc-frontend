import React, { useEffect, useState } from "react";
import { Drawer } from '@material-ui/core'
import { useTracked } from '../../service';
import Index from '../../components/view';
import colors from '../../constants/color';
import { TextTitle, TextContent } from '../../components/text';
import HomeRoundedIcon from '@material-ui/icons/HomeRounded';
import ChevronRightIcon from '@material-ui/icons/ChevronRight';
import { pageMenu, menuRegistered } from '../../constants';
import { userMenu } from "../../service/api";
import { getStorage } from "../../helpers/store";
import { decripter } from "../../../server/helper/AES";

const UCCDashboard = (props) => {

    const [state, action] = useTracked()
    const [seo, setSeo] = useState(menuRegistered[4])
    const [menu, setMenu] = useState(pageMenu)

    function initialMenu() {
        action({
            type: 'setMenu',
            menu: pageMenu[0].title
        })
    }

    async function getMenu(){
        const data = getStorage(seo.page);
        const user = decripter(data.token, data.key);
        const request = await userMenu({
            username: user.username,
            idMenu: seo.idmasterAdminAccess.toString()
        }, data.token)
        const newMenu = menu.concat(request)
        setMenu(newMenu)
    }

    useEffect(() => {   
        getMenu()     
        initialMenu()
    }, [])
    return (
        <Index menu={menu} seo={seo}>
            <div className={'flex-row-left'} style={{
                padding: 10
            }}>
                <TextTitle size={16} color={colors.dark}>{state.menu}</TextTitle>
                <div style={{
                    height: 30,
                    width: 2,
                    margin: '0 0 0 20px',
                    backgroundColor: colors.greyL
                }} />
                <ChevronRightIcon style={{
                    color: colors.grey,
                    margin: '0 20px 0 10px',
                }} />
                <TextContent color={colors.grey} size={18}>{state.subMenu}</TextContent>
            </div>
            {/* {
                state.menu === pageMenu[0].title ? (
                    <Dashboard />
                ) : (
                        state.menu === pageMenu[1].title ? (
                            <Landing menu={pageMenu[1].child} />
                        ) : (
                                <Dashboard />
                            )
                    )
            } */}
        </Index>
    )
}

export default UCCDashboard