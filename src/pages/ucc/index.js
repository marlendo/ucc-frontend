import React, { useEffect, useState } from "react"

import Layout from "../../components/layout"
import SEO from "../../components/seo"

const UCCPage = (props) => {

  const [title, setTitle] = useState('ucc')

  function getTitle(payload) {
    if (payload.state) {
      setTitle(payload.state.title)
    }
  }

  useEffect(() => {
    getTitle(props.location)
  }, [])

  return (
    <Layout>
      <SEO title={title} description={'universal campus consorsium'} />
      <h1>Undur Construction {title}</h1>
    </Layout>
  )
}

export default UCCPage
