
const colors = {
    primary: '#472D74',
    primaryD: '#4A148C',
    primaryL: '#E8EAF6',
    secondary: '#EE4323',
    secondaryD: '#b71c1c',
    thridary: '#4CAF50',
    thridaryD: '#0D47A1',
    thridaryL: '#E8F0FE',
    fourdary: '#1269DB',
    fourdaryD: '#0F5ABC',
    fourdaryL: '#EDE7F6',
    danger: '#F44336',
    grey: '#575962',
    greyD: '#424242',
    greyL: '#E0E0E0',
    dark: '#212121',
    light: '#fafafa',
    lightL: '#FFFFFF'
}

export default colors;