import graduationsBg from '../assets/images/background.jpg';
import initialPhone from '../assets/images/phone.png';
import imageNull from '../assets/images/image-null.png';
import slide1 from '../assets/images/slide1.png';
import slide2 from '../assets/images/slide2.png';
import slide3 from '../assets/images/slide3.png';
import aboutApp from '../assets/images/about-app.png';
import logo from '../assets/images/logo.png';
import siluet from '../assets/images/siluet-grad.png';

import oracle from '../assets/dummy/oracle_orig.png';
import maria from '../assets/dummy/maria.png';
import sponsore from '../assets/dummy/sponsore.png';
import ucc from '../assets/dummy/ucc.png';

// svg
import rocket from '../assets/svg/rocket.svg';
import money from '../assets/svg/money.svg';
import word from '../assets/svg/geography.svg';
import school from '../assets/svg/school.svg';
import line from '../assets/svg/line.svg';

const images = {
    initialPhone,
    imageNull,
    graduationsBg,
    slide1,
    slide2,
    slide3,
    rocket,
    money,
    word,
    school,
    line,
    aboutApp,
    logo,
    siluet,
    ucc,
    oracle,
    maria,
    sponsore
}

export default images;