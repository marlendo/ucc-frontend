import images from './images';

export const screenshotData = [
    images.slide1,
    images.slide2,
    images.slide3,
    images.slide1,
    images.slide2,
    images.slide3,
    images.slide1,
    images.slide2,
    images.slide3
]

export const sponsoreData = [
    images.ucc,
    images.sponsore,
    images.oracle,
    images.maria,
    images.ucc,
    images.sponsore,
    images.oracle,
    images.maria
]