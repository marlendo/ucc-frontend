
import React from "react";
import ArtTrackIcon from '@material-ui/icons/ArtTrack';
import DashboardIcon from '@material-ui/icons/Dashboard';

export const menuName = {
    home: 'home',
    features: 'features',
    screenshot: 'screenshot',
    team: 'team',
    download: 'download',
    sponsore: 'sponsore',
    blog: 'blog',
    contact: 'contact'
}

export const menuType = {
    sa: 'superAdmin',
    ucCamp: 'ucCamp'
}

export const initialSubPage = [
    { "idlandingMenu": 1, "landingPageMenuName": "menu", "landingPageMenuNote": "test", "landingPageMenuIcon": "test", "status": "active", "createdDate": "0000-00-00 00:00:00", "createdBy": "system", "expiredDate": "0000-00-00 00:00:00", "updatedDate": "0000-00-00 00:00:00", "updatedBy": "system", "priority": "1", "idLandingPage": "1", "idmasterAdminAccess": "1" }, 
    { "idlandingMenu": 2, "landingPageMenuName": "blog", "landingPageMenuNote": "test", "landingPageMenuIcon": "test", "status": "active", "createdDate": "0000-00-00 00:00:00", "createdBy": "system", "expiredDate": "0000-00-00 00:00:00", "updatedDate": "0000-00-00 00:00:00", "updatedBy": "system", "priority": "2", "idLandingPage": "1", "idmasterAdminAccess": "1" }, 
    { "idlandingMenu": 3, "landingPageMenuName": "register", "landingPageMenuNote": "22222222", "landingPageMenuIcon": "test2222", "status": "active", "createdDate": "0000-00-00 00:00:00", "createdBy": "uccamp", "expiredDate": "0000-00-00 00:00:00", "updatedDate": "0000-00-00 00:00:00", "updatedBy": "uccamp", "priority": "3", "idLandingPage": "1", "idmasterAdminAccess": "1" }, 
    { "idlandingMenu": 4, "landingPageMenuName": "login", "landingPageMenuNote": "test", "landingPageMenuIcon": "test", "status": "active", "createdDate": "0000-00-00 00:00:00", "createdBy": "system", "expiredDate": "0000-00-00 00:00:00", "updatedDate": "0000-00-00 00:00:00", "updatedBy": "system", "priority": "4", "idLandingPage": "1", "idmasterAdminAccess": "1" }]

export const menuRegistered = [{
    idmasterAdminAccess: 1,
    page: 'uccamp',
    desc: 'pelatihan kampus untuk dosen dan mahasiswa'
},
{
    idmasterAdminAccess: 2,
    page: 'akreditasi',
    desc: 'universal campus consorsium akreditasi'
},
{
    idmasterAdminAccess: 3,
    page: 'ucshop',
    desc: 'universal campus consorsium akreditasi ucshop'
},
{
    idmasterAdminAccess: 4,
    page: 'ucteach',
    desc: 'universal campus consorsium ucteach'
},
{
    idmasterAdminAccess: 5,
    page: 'ucjob',
    desc: 'universal campus consorsium ucjob'
},
{
    idmasterAdminAccess: 6,
    page: 'ucbiz',
    desc: 'universal campus consorsium ucbiz'
},
{
    idmasterAdminAccess: 7,
    page: 'uclearn',
    desc: 'universal campus consorsium uclearn'
},
{
    idmasterAdminAccess: 8,
    page: 'ucalumni',
    desc: 'universal campus consorsium ucalumni'
},
{
    idmasterAdminAccess: 9,
    page: 'uccsiap',
    desc: 'universal campus consorsium uccsiap'
},
]

export const mainMenu = [
    {
        id: null,
        img: <DashboardIcon className={'img-menu-logo'} />,
        title: 'Dashboard',
        child: []
    },
    {
        id: null,
        img: <ArtTrackIcon className={'img-menu-logo'} />,
        title: 'Landing Page',
        child: [
            {
                id: null,
                title: 'ucc profile',
            },
            {
                id: null,
                title: 'ucc slider',
            },
            {
                id: null,
                title: 'ucc menu',
            },
            {
                id: null,
                title: 'ucc features',
            },
            {
                id: null,
                title: 'ucc about',
            },
        ]
    }    
]

export const pageMenu = [
    {
        id: null,
        img: <DashboardIcon className={'img-menu-logo'} />,
        title: 'Dashboard',
        child: []
    }]