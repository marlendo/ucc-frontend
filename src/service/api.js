import axios from 'axios';
import { env } from '../../env';
import { encripter } from '../../server/helper/AES';
import { getStorage } from '../helpers/store';
import { getPayload } from './api-payload';

let host = 'http://localhost:8080/api/';

if(env.NODE_ENV === 'development'){
    host = 'http://ucc-edu.com/api/';
}

const header = {
    "Content-Type": "application/json",
    "Accept": "application/json"
}

const getHeaderAdmin = () => {
    return getStorage('loginsa')
}

async function request({
    method, url, headers, data
}) {
    const payload = {
        headers,
        method,
        url,
        data
    }

    const res = await axios(payload);
    return res.data;
}

export const getKey = async () => {
    try {
        const req = await axios.get(host + 'get-key')
        return req.data
    } catch (error) {
        console.log(error)
        return false
    }
}

export const getLandingsContent = async (id) => {
    try {
        const req = await axios.get(host + 'page-content/' + id)
        return req.data
    } catch (error) {
        console.log(error)
        return false
    }
}

export const getLandingMenu = async () => {
    try {
        const req = await axios.get(host + 'ucc-menu')
        return req.data.data
    } catch (error) {
        console.log(error)
        return false
    }
}

export const getLanding = async () => {
    try {
        const req = await axios.get(host + 'landing')
        return req.data.data
    } catch (error) {
        console.log(error)
        return false
    }
}

export const getImages = async () => {
    try {
        const req = await axios.get(host + 'ucc-images')
        return req.data.data
    } catch (error) {
        console.log(error)
        return false
    }
}

export const postProfile = async (data) => {
    const { token, key, jwt } = getHeaderAdmin()
    const req = await request({
        headers: {
            'Authorization': 'adminKey ' + token,
            "Content-Type": "application/json",
            "Accept": "application/json"
        },
        method: 'post',
        url: host + 'landing-profile',
        data
    })
    return req
}

export const uploadImage = async (data, type) => {
    const { token, key, jwt } = getHeaderAdmin()
    const req = await request({
        headers: {
            'Authorization': 'adminKey ' + token,            
            "imageType": type,
            'content-type': 'multipart/form-data'
        },
        method: 'post',
        url: host + 'ucc-images',
        data
    })
    return req
}

export const adminApi = async ({
    method, url, data
}) => {

    const { token } = getHeaderAdmin()
    const req = await request({
        headers: {
            'Authorization': 'adminKey ' + token,
            "Content-Type": "application/json",
            "Accept": "application/json"
        },
        method,
        url: host + url,
        data
    })
    return req
}

export const uccApi = async (data) => {
    try {
        const { token, key, jwt } = getHeaderAdmin()
        const req = await request({
            headers: {
                'Authorization': 'adminKey ' + token
            },
            method: 'post',
            url: host + 'ucc-api',
            data
        })
        return req.data.values
    } catch (error) {
        console.log(error)
        return false
    }
}

export const uccApiPrivate = async (url, param, privateToken) => {
    try {
        const data = getPayload(url, param)
        let token = '';
        if(!privateToken){
            const { token, key, jwt } = getHeaderAdmin()
            token = token;
        } else {
            token = privateToken
        }
        const req = await request({
            headers: {
                'Authorization': 'adminKey ' + token
            },
            method: 'post',
            url: host + (privateToken ? 'ucc-api-private' : 'ucc-api'),
            data
        })
        return req.data.values
    } catch (error) {
        console.log(error)
        return false
    }
}

export const userMenu = async (data, token) => {
    try {
        const req = await request({
            headers: {
                'Authorization': 'adminKey ' + token
            },
            method: 'post',
            url: host + 'user-menu',
            data: {
                url: 'inquirySubMenuEnduser',
                method: "post",
                data
            }
        })
        return req.data
    } catch (error) {
        console.log(error)
        return false
    }
}

export const uccApiPublic = async (url, param) => {
    try {
        const data = getPayload(url, param)
        const req = await request({
            headers: header,
            method: 'post',
            url: host + 'ucc-api-public',
            data
        })
        return req.data.values
    } catch (error) {
        console.log(error)
        return false
    }
}

export const loginApi = async (user, pass) => {
    try {
        const keyData = await getKey();
        if (keyData.error === false) {
            const key = keyData.data.values;
            const obj = {
                username: user,
                password: pass
            }

            const token = encripter(obj, key)
            const req = await request({
                method: 'post',
                url: host + 'login',
                headers: {
                    Authorization: 'adminKey ' + token
                },
                data: null
            })
            return req
        } else {
            return false
        }

    } catch (error) {
        console.log(error)
        return false
    }
}
