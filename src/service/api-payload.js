
export const getPayload = (url, param) => {

    switch (url) {
        case 'inquirylandingPageMenuByIdAdminAccs':
            return {
                url: url,
                method: "post",
                data: {
                    idmasterAdminAccess: param
                }
            }
        case 'inquirylandingPageHeaderByIdAdminAccs':
            return {
                url: url,
                method: "post",
                data: {
                    idmasterAdminAccess: param
                }
            }
        case 'inquirylandingPageGroupContentByIdAdminAccs':
            return {
                url: url,
                method: "post",
                data: {
                    idmasterAdminAccess: param
                }
            }
        case 'loginEnduser':
            return {
                url: url,
                method: "post",
                data: param
            }
        case 'enduserAccessMenu':
            return {
                url: url,
                method: "post",
                data: {
                    username: param
                }
            }
        case 'registerEnduser':
            return {
                url: url,
                method: "post",
                data: param
            }
        case 'giveUserAccessToMenu':
            return {
                url: url,
                method: "post",
                data: param
            }
        case 'registerUccamp':
            return {
                url: url,
                method: "post",
                data: param
            }
        case 'registerUcakreditasi':
            return {
                url: url,
                method: "post",
                data: param
            }
        case 'registerUcshop':
            return {
                url: url,
                method: "post",
                data: param
            }
        case 'registerUcteach':
            return {
                url: url,
                method: "post",
                data: param
            }
        case 'registerUcjob':
            return {
                url: url,
                method: "post",
                data: param
            }
        case 'registerUcbiz':
            return {
                url: url,
                method: "post",
                data: param
            }
        case 'registerUclearn':
            return {
                url: url,
                method: "post",
                data: param
            }
        case 'registerUcalumni':
            return {
                url: url,
                method: "post",
                data: param
            }
        case 'inquirySubMenuEnduser':
            return {
                url: url,
                method: "post",
                data: param
            }
        default:
            throw ('url not found')
    }

}