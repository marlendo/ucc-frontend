import React from 'react';
import { Provider, reducer, initialState } from '../service';
import { BarElement } from './bar';

export default ({ element }) => (
    <Provider reducer={reducer} initialState={initialState}>  
        <BarElement />     
        {element}
    </Provider>
);