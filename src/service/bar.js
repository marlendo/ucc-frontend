import React from 'react';
import { useTracked } from '../service';
import { LinearProgress, Snackbar, Drawer } from '@material-ui/core';
import { TextContent } from '../components/text';
import colors from '../constants/color';

import ErrorIcon from '@material-ui/icons/Error';
import CancelIcon from '@material-ui/icons/Cancel';
import CheckCircleIcon from '@material-ui/icons/CheckCircle';
import { ImagePickerComponent } from '../components/container';

const alertType = {
    success: 'success',
    error: 'error'
}

export const BarElement = (props) => {

    const [state, action] = useTracked();

    const handleClose = (event, reason) => {
        if (reason === 'clickaway') {
            return;
        }

        action({ type: 'closeAlert' });
    };

    return (
        <div style={{
            position: 'fixed',
            top: 0,
            width: '100%',
            zIndex: 9000
        }}>
            <Drawer anchor={'bottom'} open={state.Dopen} onClose={() => {
                action({
                    type: 'setDclose'
                })
            }}>
                <ImagePickerComponent useTracked={useTracked} />
            </Drawer>
            {state.loading ? (
                <LinearProgress color={'secondary'} />
            ) : (
                    null
                )}
            <Snackbar
                anchorOrigin={{
                    vertical: 'bottom',
                    horizontal: 'left'
                }}
                autoHideDuration={state.alert.type === alertType.success ? 1000 : 10000}
                key={'snackbar-ucc'}
                open={state.alert.active}
                onClose={handleClose}
            >
                <div style={{
                    height: 50,
                    backgroundColor: state.alert.type === alertType.success ? colors.thridary : colors.danger,
                    borderRadius: 7,
                    padding: 20,
                    boxShadow: '0 0 10px 0 #000000'
                }} className={'flex-row-left'}>
                    {
                        state.alert.type === alertType.success ? (
                            <CheckCircleIcon style={{
                                marginRight: 20,
                                color: colors.lightL
                            }} />
                        ) : (
                                <ErrorIcon style={{
                                    marginRight: 20,
                                    color: colors.lightL
                                }} />
                            )
                    }
                    <TextContent size={12} title color={colors.lightL}>
                        {
                            state.alert.message
                        }
                    </TextContent>
                    <div
                        className={'flex-row-right'}
                        style={{
                            marginLeft: 'auto'
                        }}
                        onClick={handleClose}>
                        {
                            state.alert.type === alertType.error ? (
                                <CancelIcon style={{
                                    fontSize: 30,
                                    color: colors.lightL,
                                    cursor: 'pointer',
                                    marginLeft: 20
                                }} />
                            ) : (
                                    null
                                )
                        }
                    </div>
                </div>
            </Snackbar>
        </div>
    )
}