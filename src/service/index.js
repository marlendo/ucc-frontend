// management state using hooks and provider with react tracked help
import { useReducer } from 'react';
import { createContainer } from 'react-tracked';
const useValue = ({ reducer, initialState }) => useReducer(reducer, initialState);

export const { Provider, useTracked, useTrackedState, useUpdate } = createContainer(useValue);
export const initialState = {
  alert: {
    active: false,
    type: 'error',
    message: 'unknown',
  },
  loading: false,
  menu: 'home',
  subMenu: 'home',
  image: '',
  Dopen: false,
  name: '',
  step: 0,
  mainPage: false,
  pageMenu: {
    idlandingMenu: 0
  }
};

export const reducer = (state, action) => {
  switch (action.type) {
    case 'errorAlert': return { ...state, alert: { active: true, type: 'error', message: action.message ? action.message : 'internal server error' } };
    case 'successAlert': return { ...state, alert: { active: true, type: 'success', message: action.message ? action.message : 'success' } };
    case 'closeAlert': return { ...state, alert: { ...state.alert, active: false } };
    case 'loadStart': return { ...state, loading: true };
    case 'loadStop': return { ...state, loading: false };
    case 'setMenu': return { ...state, menu: action.menu };
    case 'setSubMenu': return { ...state, subMenu: action.subMenu };
    case 'setImage': return { ...state, image: action.data };
    case 'setDopen': return { ...state, Dopen: true, name: action.name };
    case 'setDclose': return { ...state, Dopen: false };
    case 'setPageMenu': return { ...state, pageMenu: action.data };
    case 'setMainPage': return { ...state, mainPage: action.data };
    case 'setStep': return { ...state, step: action.data };
    default: throw new Error(`unknown action type: ${action.type}`);
  }
};
