import React, { useEffect } from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Stepper from '@material-ui/core/Stepper';
import Step from '@material-ui/core/Step';
import StepLabel from '@material-ui/core/StepLabel';
import Typography from '@material-ui/core/Typography';
import { getStorage } from '../helpers/store';
import { useTracked } from '../service';
import { UCCReg } from './uccRegisterPage';

export const StepperComponent = (props) => {
  const [state, action] = useTracked();

  const steps = props.steps;
  const activeStep = state.step;

  async function getLogin() {
    const data = getStorage(props.payload.page);
    if (data !== null) {
      if (data.token !== undefined) {
        action({
          type: 'setStep',
          data: 1
        })
      }
    }
  }

useEffect(() => {
    getLogin()
}, [])

  return (
    <div>
      <Stepper activeStep={activeStep} alternativeLabel style={{
        background: 'transparent'
      }}>
        {steps.map((row, i) => (
          <Step key={i}>
            <StepLabel>{row.title}</StepLabel>
          </Step>
        ))}
      </Stepper>
      <div>        
          <div style={{
            padding: 50
          }}>
            <UCCReg payload={props.content} link={props.link} />
          </div>        
      </div>
    </div>
  );
}
