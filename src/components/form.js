import React, { useState, useEffect } from 'react';
import { navigate } from "@reach/router";
import { useTracked } from '../service';
import useMediaQuery from '@material-ui/core/useMediaQuery';
import { useTheme } from '@material-ui/core/styles';
import { TextField, Grid, TextareaAutosize, Button, Checkbox } from '@material-ui/core';
import { TextContent, TextTitle } from './text';
import colors from '../constants/color';
import { menuType } from '../constants';
import { Space, Container } from './container';
import images from "../constants/images";
import { UccLogo } from "./image";
import { ButtonIcon, ButtonTabsLoginRegister } from './button';
import { Shaker } from './animation';
import { loginApi, uccApiPublic, uccApiPrivate } from '../service/api';
import { generateToken } from '../helpers/AES';

import RoomIcon from '@material-ui/icons/Room';
import PhoneIcon from '@material-ui/icons/Phone';
import EmailIcon from '@material-ui/icons/Email';
import FacebookIcon from '@material-ui/icons/Facebook';
import TwitterIcon from '@material-ui/icons/Twitter';
import InstagramIcon from '@material-ui/icons/Instagram';
import VisibilityIcon from '@material-ui/icons/Visibility';
import VisibilityOffIcon from '@material-ui/icons/VisibilityOff';
import { setStorage } from '../helpers/store';
import SearchIcon from '@material-ui/icons/Search';
import { StepperComponent } from './stepper';
import { MainRegister } from './uccRegisterPage';
import { decripter } from '../../server/helper/AES';

export const InputText = (props) => {
    return (
        <TextField
            style={{
                margin: 20
            }}
            id={props.id ? props.id : new Date().getTime()}
            label={props.label ? props.label : 'no label found'}
            variant="outlined"
            fullWidth
            name={props.name}
            value={props.value}
            onChange={props.onChange}
        />
    )
}

export const SearchComponent = (props) => {

    const [active, setActive] = useState(false);

    return (
        <div style={{
            margin: '20px 0'
        }}>
            <div style={{
                padding: 5,
                backgroundColor: active ? colors.light : colors.fourdaryD,
                borderRadius: 5,
                minWidth: active ? 350 : 250,
                transition: 'all 0.5s ease',
                border: 'none'
            }} className={'flex-row-left'}>
                <div
                    className={'flex-row-center'}
                    onClick={() => {
                        alert('search')
                    }}>
                    <SearchIcon style={{
                        fontSize: 30,
                        color: active ? colors.greyD : colors.light,
                        cursor: 'pointer',
                        marginRight: 10
                    }} />
                </div>
                <input
                    value={props.value}
                    onChange={props.onChange}
                    onBlur={() => {
                        setActive(false)
                    }}
                    onKeyDown={(e) => {
                        if (e.keyCode === 13) {
                            alert('search')
                        }
                    }}
                    onFocus={() => {
                        setActive(true)
                    }}
                    name={'search'}
                    style={{
                        fontSize: 14,
                        color: active ? colors.dark : colors.light,
                    }}
                    className={'input-general'}
                    type={'text'}
                />
            </div>
        </div>
    )
}

export const InputTextNormal = (props) => {

    const [active, setActive] = useState(false);

    return (
        <div style={{
            margin: '10px 0',
            width: '100%'
        }}>
            <TextContent title color={props.color ? props.color : colors.greyD} size={
                props.small ? 12 : 16
            }>{props.title ? props.title : props.name ? props.name : 'unknown'}</TextContent>
            <Space />
            <div style={{
                padding: 5,
                backgroundColor: colors.primaryL,
                borderRadius: 5,
                border: props.error ? '2px solid ' + colors.danger : active ? '2px solid ' + colors.primary : '2px solid transparent'
            }}>
                <input
                    onKeyDown={props.action ? props.action : null}
                    value={props.value}
                    onChange={props.onChange}
                    onBlur={() => {
                        setActive(false)
                    }}
                    onFocus={() => {
                        setActive(true)
                    }}
                    disabled={props.disabled ? props.disabled : false}
                    name={props.name ? props.name : 'unknown'}
                    style={{
                        fontSize: props.size ? props.size : 12
                    }}
                    className={'input-general'}
                    type={'text'}
                />
            </div>
            {props.error ? (
                <TextContent title color={colors.danger}>{props.errorMessage ? props.errorMessage : 'unknown error'}</TextContent>
            ) : (
                    null
                )}
        </div>
    )
}

export const InputTextPassword = (props) => {

    const [active, setActive] = useState(false);
    const [hide, setHide] = useState(true);

    return (
        <div style={{
            margin: '20px 0'
        }}>
            <TextContent title color={colors.greyD} size={16}>{props.title ? props.title : props.name ? props.name : 'unknown'}</TextContent>
            <Space />
            <div style={{
                padding: 5,
                backgroundColor: colors.primaryL,
                borderRadius: 5,
                border: props.error ? '2px solid ' + colors.danger : active ? '2px solid ' + colors.primary : '2px solid transparent'
            }} className={'flex-row-left'}>
                <input
                    value={props.value}
                    onChange={props.onChange}
                    onBlur={() => {
                        setActive(false)
                    }}
                    onKeyDown={props.action ? props.action : null}
                    onFocus={() => {
                        setActive(true)
                    }}
                    name={props.name ? props.name : 'unknown'}
                    style={{
                        fontSize: props.size ? props.size : 12
                    }}
                    className={'input-general'}
                    type={hide ? 'password' : 'text'}
                />
                <div
                    className={'flex-row-center'}
                    onClick={() => {
                        setHide(!hide)
                    }}>
                    {
                        hide ? (
                            <VisibilityOffIcon style={{
                                fontSize: 30,
                                color: colors.primary,
                                cursor: 'pointer'
                            }} />
                        ) : (
                                <VisibilityIcon style={{
                                    fontSize: 30,
                                    color: colors.primary,
                                    cursor: 'pointer'
                                }} />
                            )
                    }
                </div>
            </div>
            {props.error ? (
                <TextContent title color={colors.danger}>{'wrong password authentication'}</TextContent>
            ) : (
                    null
                )}
        </div>
    )
}

export const LoginUCCPage = (props) => {

    const [user, setUser] = useState('');
    const [pass, setPass] = useState('');

    const [state, action] = useTracked();

    function doError(message) {
        action({
            type: 'errorAlert',
            message
        })
    }

    function doSuccess(data) {
        setStorage(props.payload.page, data)
        navigate('/' + window.location.pathname.split('/')[1] + '/dashboard')
    }

    function loading(start) {
        if (start) {
            action({ type: 'loadStart' })
        } else {
            action({ type: 'loadStop' })
        }
    }

    async function doLogin() {
        loading(true)
        const obj = {
            username: user,
            password: pass
        }
        const data = await uccApiPublic('loginEnduser', obj)
        if (data) {
            loading(false)
            if (data.length > 100) {
                const token = await generateToken(obj)
                const newToken = {
                    token: token.token,
                    key: token.key,
                    jwt: data
                }
                const user = decripter(newToken.token, newToken.key);
                const getAccess = await uccApiPrivate('enduserAccessMenu', user.username, newToken.token);
                if (Array.isArray(getAccess)) {
                    const checkAccess = getAccess.filter(item => Number(item.accessMenuId) === props.payload.idmasterAdminAccess)
                    // const checkAccess = getAccess.filter(item => Number(item.accessMenuId) === 12)
                    if (checkAccess.length !== 0) {
                        doSuccess(newToken)
                        action({
                            type: 'successAlert',
                            message: 'Welcome ' + user.username
                        })
                    } else {
                        setStorage(props.payload.page, newToken)
                        action({
                            type: 'setPageMenu',
                            data: {
                                idlandingMenu: 3
                            }
                        })
                        action({
                            type: 'successAlert',
                            message: 'Complete Your Data First'
                        })
                    }
                } else {
                    setStorage(props.payload.page, newToken)
                    action({
                        type: 'setPageMenu',
                        data: {
                            idlandingMenu: 3
                        }
                    })
                    action({
                        type: 'successAlert',
                        message: 'Complete Your Data First'
                    })
                }
            } else {
                doError(data)
            }
        }
    }

    return (
        <div style={{
            width: '50vw',
            padding: 20,
            borderRadius: 15,
        }}>
            <TextTitle center>Login {props.payload.page}</TextTitle>
            <Space size={50} />
            <InputTextNormal
                value={user}
                onChange={(e) => {
                    setUser(e.target.value)
                }}
                name={'username'}
            />
            <InputTextPassword
                action={(e) => {
                    if (e.keyCode === 13) {
                        doLogin()
                    }
                }}
                value={pass}
                onChange={(e) => {
                    setPass(e.target.value)
                }}
                name={'password'}
            />
            <Button size={'large'} onClick={() => {
                doLogin()
            }} variant={'contained'} color={'primary'}>Login</Button>

        </div>
    )
}

export const LoginRegisterPage = (props) => {

    const [state, action] = useTracked();

    // async function getLogin() {
    //     const data = getStorage(props.payload.page);
    //     if (data !== null) {
    //       if (data.token !== undefined) {

    //       }
    //     }
    //   }

    // useEffect(() => {
    //     getLogin()
    // }, [])

    const steps = MainRegister(props.payload.idmasterAdminAccess)

    return (
        <div>
            <Space size={10} />
            <div style={{
                backgroundColor: colors.greyL,
                padding: 40,
                margin: 40,
                borderRadius: 15,
                boxShadow: '0 0 10px 0 ' + colors.grey
            }} className={'flex-row-center'}>
                {
                    state.pageMenu.idlandingMenu === 3 ? (
                        <StepperComponent payload={props.payload} steps={steps.steper} content={steps.content} link={steps.link} />
                    ) : (
                            <LoginUCCPage payload={props.payload} />
                        )
                }
            </div>
            <Space size={100} />
        </div>
    )
}

const TitleLoginPage = (props) => {

    return (
        <>
            <TextTitle center size={40} color={colors.primary}>
                {
                    props.type === menuType.sa ? (
                        'Super Admin Login'
                    ) : (
                            props.type === menuType.ucCamp ? (
                                'UCCAMP Login'
                            ) : (
                                    'unknown'
                                )
                        )
                }
            </TextTitle>
            <Space />
            <TextContent center size={20} color={colors.primary}>
                {
                    props.type === menuType.sa ? (
                        'Welcome Login Super Admin UCC APP Management'
                    ) : (
                            props.type === menuType.ucCamp ? (
                                'UCCAMP Login'
                            ) : (
                                    'unknown'
                                )
                        )
                }
            </TextContent>
        </>
    )
}

export const LoginPage = (props) => {

    const theme = useTheme();
    const hide = useMediaQuery(theme.breakpoints.down('xs'));

    return (
        <Container>
            <Grid container
                direction="row"
                justify="center"
                alignItems="flex-start" >

                <Grid item sm style={{
                    height: hide ? '40%' : '100%',
                    width: '100%'
                }}>
                    <div style={{
                        height: '100%',
                        width: '100%',
                        backgroundColor: colors.light,
                        padding: 20
                    }}
                        className={'flex-column-center'}>
                        <UccLogo
                            img={
                                images.logo
                            }
                            size={hide ? '50vw' : 300}
                        />
                        <Space size={30} />
                        <TitleLoginPage type={props.type} />
                    </div>
                </Grid>
                <Grid item sm style={{
                    height: '100%',
                    width: '100%'
                }}>
                    <div style={{
                        height: '100%',
                        width: '100%',
                        backgroundColor: colors.primary
                    }} className={'flex-column-center'}>
                        <FormLogin type={props.type} />
                    </div>
                </Grid>
            </Grid>
        </Container>
    )
}

export const FormLogin = (props) => {

    const theme = useTheme();
    const hide = useMediaQuery(theme.breakpoints.down('xs'));

    const [state, action] = useTracked();

    const [email, setEmail] = useState('');
    const [pass, setPass] = useState('');
    const [remember, setRemember] = useState(false);
    const [shake, setShake] = useState(0);

    const [error, setError] = useState('');

    function doError(message) {
        setShake(shake + 1);
        action({
            type: 'errorAlert',
            message
        })
    }

    function doSuccess(data) {
        setStorage('loginsa', data)
        window.location.reload();
    }

    function loading(start) {
        if (start) {
            setError('')
            action({ type: 'loadStart' })
        } else {
            action({ type: 'loadStop' })
        }
    }

    async function doLogin() {
        try {
            // simulateLogin()
            loading(true)
            const data = await loginApi(email, pass)
            if (data.error) {
                loading(false)
                if (data.data.values) {
                    doError('Username or Password Invalid')
                } else {
                    doError('Internal Server Error')
                }
            } else {
                loading(false)
                doSuccess(data.data)
            }
        } catch (error) {
            console.log(error)
            loading(false)
            doError('Internal Server Error')
        }
    }

    return (
        <Shaker doShake={shake}>
            <div style={{
                boxShadow: state.alert.active ? '0 0 10px 1px ' + colors.danger : '0 0 5px 0 ' + colors.greyD,
                padding: 20,
                borderRadius: 14,
                minWidth: hide ? '80vw' : '35vw',
                backgroundColor: colors.light
            }} className={'flex-column-center'}>
                <Space size={50} />
                <TextTitle center color={colors.greyD}>
                    Sign In To {
                        props.type === menuType.sa ? 'Super Admin' : menuType.ucCamp ? 'UCCAMP' : 'unknown'
                    }
                </TextTitle>
                <div style={{
                    width: '100%',
                    padding: 50
                }}>
                    <InputTextNormal
                        action={(e) => {
                            if (e.keyCode === 13) {
                                doLogin()
                            }
                        }}
                        value={email}
                        onChange={(e) => {
                            setEmail(e.target.value)
                        }}
                        error={error === 'email'}
                        name={'email'}
                        errorMessage={'wrong email authentication'}
                    />
                    <InputTextPassword
                        action={(e) => {
                            if (e.keyCode === 13) {
                                doLogin()
                            }
                        }}
                        value={pass}
                        onChange={(e) => {
                            setPass(e.target.value)
                        }}
                        error={error === 'pass'}
                        name={'password'}
                    />
                    <Space />
                    <div className={'flex-row-center'}>
                        <div className={'flex-row-left'}>
                            <Checkbox
                                color='primary'
                                checked={remember}
                                onChange={() => {
                                    setRemember(!remember)
                                }}
                                name="checked"
                            />
                            <TextContent>Remember Me ?</TextContent>
                        </div>
                        <Button
                            onClick={() => {
                                doLogin()
                            }}
                            variant="contained"
                            color="primary"
                            style={{
                                padding: '10px 50px'
                            }}
                        >
                            <TextTitle color={colors.light} size={14}>Login</TextTitle>
                        </Button>
                    </div>
                </div>
            </div>
        </Shaker>
    )
}

const Address = (props) => {

    return (
        <div className={'flex-row-left'} style={{
            margin: 2,
        }}>
            {props.children}
        </div>
    )

}

export const FormContact = (props) => {

    const [name, setName] = useState('');
    const [email, setEmail] = useState('');
    const [message, setMessage] = useState('');

    const theme = useTheme();
    const hide = useMediaQuery(theme.breakpoints.down('sm'));

    return (
        <Grid container
            direction="row"
            justify="center"
            alignItems="center">
            <Grid item md style={{
                padding: 0,
                maxWidth: hide ? '80vw' : null,
                margin: hide ? null : '0 10vw'
            }}>
                <form noValidate autoComplete="on">
                    <InputText
                        id={'name'}
                        label={'name'}
                        name={'name'}
                        value={name}
                        onChange={(e) => {
                            setName(e.target.value)
                        }}
                    />
                    <InputText
                        id="input-email"
                        label="Email"
                        name={'email'}
                        value={email}
                        onChange={(e) => {
                            setEmail(e.target.value)
                        }}
                    />
                    <TextareaAutosize
                        style={{
                            margin: 20,
                            width: '100%',
                            padding: '20px 15px',
                            height: 200,
                            borderColor: colors.grey,
                            borderRadius: 4
                        }}
                        aria-label="Message"
                        placeholder="Message"
                        value={message}
                        onChange={(e) => {
                            setMessage(e.target.value)
                        }}
                    />
                </form>
            </Grid>
            <AddressComponent payload={props.payload} />
        </Grid>
    );
}

export const AddressComponent = (props) => {

    const theme = useTheme();
    const hide = useMediaQuery(theme.breakpoints.down('sm'));

    return (
        <Grid item md className={'flex-column-center'} style={{
            padding: 50,
            maxWidth: hide ? '100vw' : null,
            margin: hide ? '0 10vw' : null
        }}>
            <Address>
                <RoomIcon style={{
                    fontSize: 30,
                    color: colors.primary,
                    margin: 10
                }} />
                <TextContent size={16} color={colors.greyD}>
                    {
                        props.payload.address
                    }
                </TextContent>
            </Address>
            <Address>
                <PhoneIcon style={{
                    fontSize: 30,
                    color: colors.primary,
                    margin: 10
                }} />
                <div>
                    {
                        props.payload.phone.map((row, i) => (
                            <div key={i}>
                                {
                                    i !== 0 ? (
                                        <Space />
                                    ) : (
                                            null
                                        )
                                }
                                <TextContent size={16} color={colors.greyD}>
                                    {row}
                                </TextContent>
                            </div>
                        ))
                    }
                </div>
            </Address>
            <Space size={10} />
            <Address>
                <EmailIcon style={{
                    fontSize: 30,
                    color: colors.primary,
                    margin: 10
                }} />
                <div>
                    {
                        props.payload.email.map((row, i) => (
                            <div key={i}>
                                {
                                    i !== 0 ? (
                                        <Space />
                                    ) : (
                                            null
                                        )
                                }
                                <TextContent size={16} color={colors.greyD}>
                                    {row}
                                </TextContent>
                            </div>
                        ))
                    }
                </div>
            </Address>
            <div className={'flex-row-center'} style={{
                padding: 50,
            }}>
                <ButtonIcon
                    onClick={() => {
                        window.open(props.payload.fb, '_blank');
                    }}
                    icon={
                        <FacebookIcon style={{
                            fontSize: 50,
                            color: colors.light,
                        }} />
                    } />
                <ButtonIcon
                    onClick={() => {
                        window.open(props.payload.ig, '_blank');
                    }}
                    icon={
                        <InstagramIcon style={{
                            fontSize: 50,
                            color: colors.light,
                        }} />
                    } />
                <ButtonIcon
                    onClick={() => {
                        window.open(props.payload.twet, '_blank');
                    }}
                    icon={
                        <TwitterIcon style={{
                            fontSize: 50,
                            color: colors.light,
                        }} />
                    } />
            </div>
        </Grid>
    )
}
