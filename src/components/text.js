import React from 'react';
import useMediaQuery from '@material-ui/core/useMediaQuery';
import { useTheme } from '@material-ui/core/styles';
import colors from '../constants/color';


export const TextTitle = (props) => {
    const theme = useTheme();
    const hide = useMediaQuery(theme.breakpoints.down('sm'));
    const originSize = props.size ? props.size : 20;
    const size = hide ? originSize - 7 : originSize

    return (
        <h1
            className={'text-title'}
            style={{
                fontSize: size < 17 ? 16 : size,
                color: props.color ? props.color : colors.dark,
                textAlign: props.center ? 'center' : props.textAlign ? props.textAlign : 'left',
                textTransform: props.title ? 'uppercase' : 'capitalize'
            }}>{props.children}</h1>
    );
}

export const TextContent = (props) => {
    const theme = useTheme();
    const hide = useMediaQuery(theme.breakpoints.down('sm'));
    const originSize = props.size ? props.size : 12;
    const size = hide ? originSize - 3 : originSize

    return (
        <p
            className={'text-content'}
            style={{
                fontSize: size < 11 ? 10 : size,
                color: props.color ? props.color : colors.dark,
                textAlign: props.center ? 'center' : props.textAlign ? props.textAlign : 'left',
                lineHeight: props.lineHeight ? props.lineHeight : null,
                textTransform: props.title ? 'capitalize' : null,
                fontWeight: props.title ? 'bold' : null
            }}>{props.children}</p>
    );
}