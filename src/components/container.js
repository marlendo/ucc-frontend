import React, { useState, useEffect } from 'react';
import { navigate } from "@reach/router";
import useMediaQuery from '@material-ui/core/useMediaQuery';
import { useTheme } from '@material-ui/core/styles';
import { TextTitle, TextContent } from "./text";
import { Grid } from '@material-ui/core';
import { LeaderCard, TitleCard, FeaturesCard } from "../components/card";
import images from '../constants/images';
import colors from '../constants/color';
import { adminContent, menuRegistered } from '../constants';
import { ButtonDownload } from "../components/button";

import { getImages, uploadImage, uccApiPublic, getLandingsContent } from '../service/api';

import AppleIcon from '@material-ui/icons/Apple';
import AndroidIcon from '@material-ui/icons/Android';
import AddAPhotoIcon from '@material-ui/icons/AddAPhoto';
import { validURL } from '../helpers/validator';

export const Space = (props) => {

    return (
        <div style={{
            height: props.size ? props.size : 10,
            width: '100%'
        }} />
    );
}

export const Container = (props) => {

    return (
        <div style={{
            minHeight: '100vh',
            minWidth: '100vw',
            display: 'flex',
            flex: 1
        }}>
            {props.children}
        </div>
    );
}

export const MenuHeader = (props) => {

    const [focus, setFocus] = useState(false)

    return (
        <div className={'layout-menu-header'}
            onMouseMove={() => {
                setFocus(true)
            }}
            onMouseLeave={() => {
                setFocus(false)
            }}
            onClick={props.onClick}
        >
            <TextTitle size={
                focus ? 18 : 16
                } color={
                    focus ? colors.secondary : props.active ? colors.secondary : colors.primary
                    }>{props.title}</TextTitle>
            <div style={{
                width: '100%',
                height: 3,
                backgroundColor: focus ? colors.secondary : props.active ? colors.secondary : 'transparent',
                marginTop: 5
            }} />
        </div>
    );
}

export const MenuDrawer = (props) => {

    const [focus, setFocus] = useState(false)

    return (
        <div className={'layout-menu-header'}
            onMouseMove={() => {
                setFocus(true)
            }}
            onMouseLeave={() => {
                setFocus(false)
            }}
            onClick={props.onClick}
        >
            <div style={{
                width: '100%',
                backgroundColor: props.active ? colors.secondary : focus ? colors.secondary : 'transparent',
                marginTop: 5,
                padding: 10
            }}>
                <TextTitle size={16} color={props.active ? colors.light : focus ? colors.light : colors.primary}>{props.title}</TextTitle>
            </div>
        </div>
    );
}

export const UCCMenu = (props) => {

    const theme = useTheme();
    const hide = useMediaQuery(theme.breakpoints.down('sm'));
    const [height, setHeight] = useState(100)

    function goPage(row) {
        const data = menuRegistered.filter(item => item.idmasterAdminAccess === row.idmasterAdminAccess)
        if (data.length !== 0) {
            const page = data[0].page
            navigate('/' + page)
        } else {
            navigate('/ucc', { state: row })
        }
    }

    useEffect(()=> {
        const sss = hide ? 2 : 1
        const ssss = hide ? 200 : 0
        setHeight((window.document.getElementById('mission').offsetHeight / sss) - ssss )
    }, [])

    return (
        <div className={'layout-container'} style={{
            height: height,            
        }}>
            <div id={'mission'} className={'layout-mission'}>
                <div style={{
                    display: 'flex',
                    flexWrap: 'wrap',
                    justifyContent: 'center',
                    alignItems: 'center',
                    flex: 1,
                    padding: hide ? 0 : 50
                }}>
                    {
                        props.payload.map((row, i) => (
                            <div style={{
                                display: 'flex',
                                margin: hide ? 5 : '5px 20px'
                            }}
                                key={i}
                                onClick={() => {
                                    goPage(row)
                                }}
                            >
                                <LeaderCard
                                    image={row.img}
                                    title={row.title}
                                />
                            </div>
                        ))
                    }
                </div>
            </div>
        </div>
    )
}

export const PageContent = (props) => {

    const nopadding = props.padding === false ? true : false;

    return (
        <div className={'layout-page'} style={{
            background: props.transparent ? 'transparent' : null
        }}>
            {props.id ? (
                <div id={props.id} />
            ) : (
                    null
                )}
            {props.title ? (
                <TitleCard title={props.title} />
            ) : (
                    null
                )}
            <div style={{
                padding: nopadding ? 0 : '0 20px',
            }}>
                {props.children}
            </div>
        </div>
    )
}

export const AboutCompoment = (props) => {

    const content = props.payload
    const overide = props.overide

    return (
        <Grid container
            direction="row"
            justify="center"
            alignItems="center" >
            <Grid item md className={'flex-row-center'}>
                {
                    overide ? overide.id === 'about' && overide.img !== '' ? (
                        <img
                            src={overide.img}
                            className={'img-feature'}
                            alt={'about-ucc-app'} />
                    ) : (
                            <img
                                className={'img-feature'}
                                src={content.img === null ? images.initialPhone : content.img}
                                alt={'about-ucc-app'} />
                        ) : (
                            content ? (
                                <img
                                    className={'img-feature'}
                                    src={content.img === null ? images.initialPhone : content.img}
                                    alt={'about-ucc-app'} />
                            ) : (
                                    null
                                )
                        )
                }
            </Grid>
            <Grid item md style={{
                width: '50vw',
                marginLeft: 20
            }}>
                {
                    content ?
                        content.content.map((row, i) => (
                            <div key={i}>
                                {i !== 0 ? (
                                    <Space size={20} />
                                ) : (
                                        null
                                    )}
                                <TextContent
                                    size={14}
                                    color={colors.greyD}
                                    lineHeight={2}
                                >
                                    {row}
                                </TextContent>
                            </div>
                        ))
                        : null
                }
            </Grid>
        </Grid>
    )
}

export const FeatureComponent = (props) => {

    const content = props.payload
    const overide = props.overide

    return (
        content ? (
            <Grid container
                direction="row"
                justify="center"
                alignItems="center">
                <Grid item md className={'flex-column-center'}>
                    {
                        content.left.map((row, i) => (
                            <div key={i}>
                                <FeaturesCard
                                    title={row.title}
                                    content={row.content}
                                    icon={row.icon}
                                />
                            </div>
                        ))
                    }
                </Grid>
                <Grid item md className={'flex-row-center'}>
                    {
                        overide ? overide.image !== '' && overide.id === 'feature' ? (
                            <img src={
                                overide.img
                            }
                                className={'img-feature'}
                                alt={'features-ucc-app'}
                            />
                        ) : (
                                <img src={
                                    content.img === null ? images.initialPhone : content.img}
                                    alt={'features-ucc-app'}
                                    className={'img-feature'}
                                />
                            ) : (
                                <img src={
                                    content.img === null ? images.initialPhone : content.img}
                                    alt={'features-ucc-app'}
                                    className={'img-feature'}
                                />
                            )
                    }
                </Grid>
                <Grid item md className={'flex-column-center'}>
                    {
                        content.right.map((row, i) => (
                            <div key={i}>
                                <FeaturesCard
                                    left
                                    title={row.title}
                                    content={row.content}
                                    icon={row.icon
                                    }
                                />
                            </div>
                        ))
                    }
                </Grid>
            </Grid>
        ) : (
                null
            )
    )
}

export const StatisticContent = (props) => {

    return (
        <div style={{
            backgroundColor: colors.dark,
            position: 'relative',
            height: 300
        }}>
            <img src={images.siluet}
                style={{
                    height: '100%',
                    width: '100%',
                    objectFit: 'cover',
                    opacity: '0.2',
                    margin: 0,
                    padding: 0
                }}
                alt={'bg-siluet-graduation'}
            />
            <div style={{
                height: '100%',
                width: '100%',
                position: 'absolute',
                top: 0
            }}>
                <div className={'flex-row-center'} style={{
                    height: '100%',
                    flexWrap: 'wrap'
                }}>
                    {props.children}
                </div>
            </div>
        </div>
    )
}

export const GetAppContent = (props) => {

    const theme = useTheme();
    const hide = useMediaQuery(theme.breakpoints.down('sm'));

    return (
        <div style={{
            minHeight: 400,
            backgroundImage: hide ? 'url(' + images.graduationsBg + ')' : null
        }} className={'flex-row-center'}>
            <div className={'flex-column-center'} style={{
                backgroundColor: 'rgba(0,0,0,0.7)',
                padding: 20,
                minHeight: 400,
                width: '100%'
            }}>
                <div>
                    <TextTitle color={colors.light} size={50} center>Get App</TextTitle>
                    <Space />
                    <TextContent color={colors.light} center>
                        Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla pellentesque.
              </TextContent>
                </div>
                <Grid container
                    direction="row"
                    justify="center"
                    alignItems="center">
                    <Grid item md className={'flex-column-center'}>
                        <ButtonDownload
                            title={'Apple IOS'}
                            icon={
                                <AppleIcon style={{
                                    fontSize: 70,
                                    color: colors.secondary
                                }} />
                            } />
                    </Grid>
                    <Grid item md className={'flex-column-center'}>
                        <ButtonDownload
                            title={'Android'}
                            icon={
                                <AndroidIcon style={{
                                    fontSize: 70,
                                    color: colors.secondary
                                }} />
                            } />
                    </Grid>
                </Grid>
            </div>
        </div>
    )
}

export const ImagePickerComponent = (props) => {

    const [state, action] = props.useTracked()

    const [active, setActive] = useState('image');
    const [imgFile, setImgFile] = useState(false);
    const [imgList, setImgList] = useState([]);

    const image = [
        'image',
        'background',
        'icon'
    ]

    async function doUpload(file) {
        const formData = new FormData();
        formData.append('image', file);
        const data = await uploadImage(formData, active)
        if (data.error === false) {
            listImage()
            alert('success add image')
        }
    }

    async function listImage() {
        const data = await getImages()
        setImgList(data)
    }

    useEffect(() => {
        listImage()
    }, [])

    return (
        <div>
            <div style={{
                backgroundColor: colors.primary,
                padding: 20
            }}>
                <TextTitle color={colors.light}>Assets Dictionary</TextTitle>
            </div>
            <div className={'flex-column-left'} style={{
                padding: 10
            }}>
                <div className={'flex-row-left'}>
                    {
                        image.map((row, i) => (
                            <div
                                key={i}
                                onClick={() => {
                                    setActive(row)
                                }}
                                className={active === row ? 'button-type-active' : 'button-type'}>
                                <TextContent color={colors.light}>{row}</TextContent>
                            </div>
                        ))
                    }
                    <input
                        type="file"
                        id={"button-upload-image"}
                        style={{ display: 'none' }}
                        onChange={(e) => {
                            if (e.target.files[0]) {
                                const image = {
                                    preview: URL.createObjectURL(e.target.files[0]),
                                    raw: e.target.files[0]
                                }
                                setImgFile(image)
                                doUpload(e.target.files[0])
                            }
                        }}
                    />
                    <label htmlFor={"button-upload-image"}
                        style={{
                            maxWidth: 300,
                            marginLeft: 'auto'
                        }}
                        className={'button-type flex-row-center'}>
                        <AddAPhotoIcon style={{
                            color: colors.light,
                            fontSize: 50,
                            margin: '0 20px',
                        }} />
                        <TextContent title color={colors.light}>Add {active}</TextContent>
                    </label>
                </div>
                <div style={{
                    width: '100%',
                    overflow: 'auto',
                    whiteSpace: 'nowrap'
                }}>
                    {
                        imgFile ? (
                            <div style={{
                                display: 'inline-block',
                                backgroundColor: colors.grey,
                                margin: 10,
                                height: active === 'image' ? 150 : active === 'background' ? 200 : 100,
                                width: active === 'image' ? 150 : active === 'background' ? 200 : 100,
                            }}>
                                <img
                                    style={{
                                        width: '100%',
                                        height: '100%',
                                        margin: 0,
                                    }}
                                    src={imgFile.preview}
                                    alt={'preview-image'}
                                />
                            </div>
                        ) : (
                                null
                            )
                    }
                    {
                        imgList.filter(item => item.type === active)
                            .map((row, i) => (
                                <div
                                    onClick={() => {
                                        action({
                                            type: 'setImage',
                                            data: row.url
                                        })
                                        action({
                                            type: 'setDclose'
                                        })
                                    }}
                                    key={i}
                                    style={{
                                        cursor: 'pointer',
                                        display: 'inline-block',
                                        backgroundColor: colors.grey,
                                        margin: 10,
                                        height: active === 'image' ? 150 : active === 'background' ? 200 : 100,
                                        width: active === 'image' ? 150 : active === 'background' ? 200 : 100,
                                    }}>
                                    <img
                                        style={{
                                            width: '100%',
                                            height: '100%',
                                            margin: 0,
                                        }}
                                        src={row.url}
                                        alt={row.name}
                                    />
                                </div>
                            ))}
                </div>
            </div>
        </div>
    )
}

export const HeaderComponent = (props) => {

    return (
        <div style={{
            height: 400
        }}>
            <div style={{
                position: 'relative'
            }}>
                {validURL(props.landingPageHeaderImage) ? (
                    <img src={props.landingPageHeaderImage} alt={'ucc-landing-page-title'} style={{
                        width: '100%',
                        height: 400,
                        margin: 0,
                        marginBottom: -10,
                        objectFit: 'cover'
                    }} />
                ) : (
                        // <div style={{
                        //     width: '100%',
                        //     height: 400,
                        //     margin: 0,
                        //     marginBottom: -10,
                        //     objectFit: 'cover',
                        //     backgroundColor: colors.grey
                        // }} />
                        <img src={'https://rencanamu.id/assets/file_uploaded/blog/1465132294-shuttersto.jpg'} alt={'ucc-landing-page-title'} style={{
                            width: '100%',
                            height: 400,
                            margin: 0,
                            marginBottom: -10,
                            objectFit: 'cover'
                        }} />
                    )}
                <div style={{
                    width: 'calc(100% - 40px)',
                    marginLeft: 20,
                    height: 'calc(100% - 30px)',
                    backgroundColor: 'rgb(0, 0, 0, 0.5)',
                    position: 'absolute',
                    left: 0,
                    top: 20
                }}>
                    <div style={{
                        height: '100%',
                        width: '100%'
                    }} className={'flex-row-center'}>
                        <div>
                            <TextTitle title size={50} center color={colors.light}>{props.payload.landingPageHeaderName}</TextTitle>
                            <div style={{
                                height: 2,
                                width: '100%',
                                backgroundColor: colors.light,
                                marginTop: 5,
                                marginBottom: 5,
                            }} />
                            <TextTitle size={20} center color={colors.light}>{props.payload.landingPageHeaderNote}</TextTitle>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    )
}

const Template1 = (props) => {

    const theme = useTheme();
    const hide = useMediaQuery(theme.breakpoints.down('sm'));

    const cRow = props.cRow;

    return (
        <div className={'flex-row-center'}>
            {/* <img src={props.landingPageHeaderImage} alt={'ucc-landing-content'} style={{
                height: 300,
                flex: 2,
                borderRadius: 15,
                objectFit: 'cover'
            }} /> */}
            <div style={{
                height: 300,
                backgroundColor: colors.grey,
                flex: 2,
                borderRadius: 15
            }} />
            <div style={{
                width: 20
            }} />
            <div style={{
                flex: 3
            }}>
                <TextTitle size={25}>{cRow.landingPageContentName}</TextTitle>
                <Space />
                <TextContent size={14}>{cRow.landingPageContentNote}</TextContent>
            </div>
        </div>
    )
}

const Template2 = (props) => {

    const theme = useTheme();
    const hide = useMediaQuery(theme.breakpoints.down('sm'));

    const cRow = props.cRow;

    return (
        <div className={'flex-row-center'}>
            <div style={{
                flex: 3
            }}>
                <TextTitle size={25}>{cRow.landingPageContentName}</TextTitle>
                <Space />
                <TextContent size={14}>{cRow.landingPageContentNote}</TextContent>
            </div>
            <div style={{
                width: 20
            }} />
            <div style={{
                height: 300,
                backgroundColor: colors.grey,
                flex: 2,
                borderRadius: 15
            }} />
        </div>
    )
}

export const ContentComponent = (props) => {

    const [content, setContent] = useState([])
    const [header, setHeader] = useState({
        landingPageHeaderName: null,
        landingPageHeaderNote: null
    })

    async function getHeader() {
        const data = await uccApiPublic('inquirylandingPageHeaderByIdAdminAccs', props.id)
        setHeader(data[0])
    }

    async function getContent() {
        const data = await getLandingsContent(props.id)
        setContent(data.data)
    }

    useEffect(() => {
        getHeader()
        getContent()
    }, [])

    return (
        <div>
            <HeaderComponent payload={header} />
            <div style={{
                padding: 60
            }}>
                {
                    content.map((row, i) => ( 
                        <div key={i}>
                            <div>
                                <TextTitle size={30}>{row.GroupContentName}</TextTitle>
                            </div>
                            {
                                Array.isArray(row.child) ? 
                                row.child.map((cRow, cI) => (
                                    <div key={cI} style={{
                                        padding: 40
                                    }}>
                                        {
                                            row.idTypeTemplate === '1' ? (
                                                <Template1 cRow={cRow} />
                                            ) : (
                                                    <Template2 cRow={cRow} />
                                                )
                                        }
                                    </div>
                                )) : null
                            }
                            <Space size={50} />
                        </div>
                    ))
                }
            </div>
        </div>
    )
}