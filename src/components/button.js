import React, { useEffect } from 'react';
import { TextTitle, TextContent } from './text';
import colors from '../constants/color';
import { useTracked } from '../service';

export const ButtonDownload = (props) => {
    return (
        <div className={'flex-row-center button-download'}>
            <div style={{
                width: 100,
                height: 100,
                backgroundColor: colors.light,
                borderRadius: 100,
                display: 'flex',
                alignItems: 'center',
                justifyContent: 'center'
            }}>
                {props.icon}
            </div>
            <div style={{
                margin: '0 20px'
            }}>
                <TextContent color={colors.light} size={20}>
                    Download For
                </TextContent>
                <TextTitle color={colors.light} size={30}>{props.title}</TextTitle>
            </div>
        </div>
    );
}

export const ButtonIcon = (props) => {
    return (
        <div className={'flex-row-center button-icon'} onClick={props.onClick}>
            {props.icon}
        </div>
    );
}

export const ButtonTabsLoginRegister = (props) => {

    const [state, action] = useTracked();

    useEffect(() => {
        console.log(state.pageMenu)
    }, [])

    return (
        <div className={'flex-row-center'}>
            <div style={{
                flexDirection: 'row',
                display: 'flex'
            }}>
                <div 
                className={state.pageMenu.idlandingMenu === 3 ? 'button-left-active' : 'button-left'}
                onClick={()=> {
                    action({
                        type: 'setPageMenu',
                        data: {
                            idlandingMenu: 3
                        }
                    })
                }}
                >
                    <TextTitle color={colors.light}>Register</TextTitle>
                </div>
                <div 
                onClick={()=> {
                    action({
                        type: 'setPageMenu',
                        data: {
                            idlandingMenu: 4
                        }
                    })
                }}
                className={state.pageMenu.idlandingMenu === 4 ? 'button-right-active' : 'button-right'}>
                    <TextTitle color={colors.light}>Login</TextTitle>
                </div>
            </div>
        </div>
    )
}
