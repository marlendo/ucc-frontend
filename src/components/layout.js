import React from "react";
import PropTypes from "prop-types";
import { useStaticQuery, graphql } from "gatsby";

import Header from "./header";
import "./layout.css";
import "../styles/ucc.css";

const Layout = ({ children }) => {

  const data = useStaticQuery(graphql`
    query SiteTitleQuery {
      site {
        siteMetadata {
          title
        }
      }
    }
  `)

  return (
    <div style={{
      minHeight: '100vh'
    }}>
      <Header siteTitle={data.site.siteMetadata.title} />
      <div style={{
        height: 70,
        width: '100%'
      }} />
      <main>{children}</main>
      <div className={'flex-column-end'}>
        <div style={{
        backgroundColor: '#472d74',
        padding: 20,
        textAlign: 'center',
        color: '#FFFFFF',
        width: '100%'
      }}>
          © {new Date().getFullYear()} <small style={{
            textDecoration: 'underline'
          }}>Universal Campus Consortium</small>
        </div>
      </div>
    </div>
  )
}

Layout.propTypes = {
  children: PropTypes.node.isRequired,
}

export default Layout
