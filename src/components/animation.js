import React, { Component, useEffect } from "react";
import useMediaQuery from '@material-ui/core/useMediaQuery';
import { useTheme } from '@material-ui/core/styles';
import { Slide } from "react-reveal";
import Shake from 'react-reveal/Shake';
import { sliderApp, screenshotData, sponsoreData } from '../constants/banner';
import 'react-alice-carousel/lib/alice-carousel.css';
import AliceCarousel from 'react-alice-carousel';
import colors from "../constants/color";
import images from '../constants/images';

class SliderClass extends Component {
    constructor(props) {
        super(props);
        this.state = {
            show: true,
            count: 0,
            index: 0,
        };
    }

    componentDidMount() {        
        this.interval = setInterval(() => {
            this.setState({ count: this.state.count + 1 });
            if (this.state.count > 9) {
                this.setState({
                    show: false,
                    count: 0
                })
            } else {
                if (!this.state.show) {
                    this.setState({
                        show: true,
                        index: this.state.index >= this.props.payload.length - 1 ? 0 : this.state.index + 1
                    })
                }
            }
        }, 1000);
    }

    componentWillUnmount() {
        clearInterval(this.interval);
    }

    render() {
        return (
            <>
                <Slide left cascade big when={this.state.show}>
                    <img src={
                        this.props.payload[this.state.index].img === null ? images.initialPhone : this.props.payload[this.state.index].img
                        } alt={'ucc-design-dashboard'} 
                        className='img-banner-iphone' />
                </Slide>
                <Slide right cascade big when={this.state.show}>
                    <div className='layout-banner'>
                        <p className='text-title-banner'>
                            {this.props.payload[this.state.index].text.title}
                        </p>
                        {/* <hr className='hr-slider' /> */}
                        {this.props.payload[this.state.index].text.content.map((row, i) => (
                            <p className='text-subtitle-banner' key={i.toString()}>{row}</p>
                        ))}
                    </div>
                </Slide>
            </>
        )
    }
}

export const Shaker = (props) => {

    useEffect( () => {}, 
    [props.doShake])

    return (
        <Shake spy={props.doShake}>
            {props.children}
        </Shake>
    )
}

export const Slider = (props) => {

    const theme = useTheme();
    const hide = useMediaQuery(theme.breakpoints.down('sm'));

    return (
        hide ? null : (
            <div className='full-vh'>
                <SliderClass payload={props.payload} />
            </div>
        )
    )
}

export const Screenshot = (props) => {

    const handleOnDragStart = (e) => e.preventDefault()

    const responsive = {
        0: { items: 1 },
        768: { items: 2 },
        992: { items: 3 },
        1200: { items: 4 }
    }

    const Items = screenshotData.map((row, i) => (
        <img src={row} onDragStart={handleOnDragStart} alt={'ucc-scc-' + i}
            style={{
                width: 200
            }} />
    ))

    return (
        <div style={{
            padding: '50px 0px',
            backgroundColor: colors.light
        }}>
            <AliceCarousel
                items={Items}
                responsive={responsive}
                autoPlay
                buttonsDisabled
                // dotsDisabled
                autoPlayInterval={5000}
                stagePadding={{
                    paddingLeft: 20,
                    paddingRight: 100
                }}
                mouseTrackingEnabled
            />
        </div>
    )
}

export const Sponsore = (props) => {

    const handleOnDragStart = (e) => e.preventDefault()

    const responsive = {
        0: { items: 1 },
        1200: { items: 2 }
    }

    const Items = sponsoreData.map((row, i) => (
        <img src={row} onDragStart={handleOnDragStart} alt={'ucc-support-' + i}
            style={{
                width: '40vw',
                margin: '0 20px'
            }} />
    ))

    return (
        <div style={{
            padding: '50px 0px',
            backgroundColor: colors.light
        }}>
            <AliceCarousel
                items={Items}
                responsive={responsive}
                autoPlay
                buttonsDisabled
                dotsDisabled
                autoPlayInterval={5000}
                stagePadding={{
                    paddingLeft: 20,
                    paddingRight: 100
                }}
                mouseTrackingEnabled
            />
        </div>
    )
}