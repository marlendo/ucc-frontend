import React from 'react';
import useMediaQuery from '@material-ui/core/useMediaQuery';
import { useTheme } from '@material-ui/core/styles';
import { Grid } from '@material-ui/core';
import { TextTitle, TextContent } from '../components/text';
import { Space } from './container';
import images from '../constants/images';
import colors from '../constants/color';

export const LeaderCard = (props) => {
    const theme = useTheme();
    const hide = useMediaQuery(theme.breakpoints.down('sm'));

    return (
        <div className={props.active ? 'paper-home-active' : 'paper-home'}>
            <img
                style={{
                    width: props.size ? props.size : hide ? 70 : 100,
                    height: props.size ? props.size : hide ? 70 : 100,
                    objectFit: 'fill',
                    margin: '0 10px 20px 10px'
                }}
                src={props.image === null ? images.imageNull : props.image}
                alt={props.image}
            />
            <TextTitle size={20} center>{props.title}</TextTitle>
        </div>
    );
}

export const TitleCard = (props) => {
    return (
        <div style={{
            height: 200,
            display: 'flex',
            justifyContent: 'center',
            alignItems: 'center',
            backgroundColor: colors.light
        }}>
            <div>
                <TextTitle center color={colors.secondary} size={30}>{props.title}</TextTitle>
                <Space size={10} />
                <img
                    style={{
                        width: 600,
                        height: 50,
                        margin: 0,
                    }}
                    src={images.line}
                    alt={'props.image'}
                />
            </div>
        </div>
    )
}

export const FeaturesCard = (props) => {

    return (
        <>
            {props.left ? (
                <div className={'flex-row-center'} style={{
                    margin: '20px 0px'
                }}>
                    <Grid item xs={2}>
                        <div className={'logo-header-layout'}>
                            <img style={{
                                width: 35,
                                height: 35,
                                objectFit: 'cover',
                                margin: 0
                            }} src={props.icon === null ? images.imageNull : props.icon} alt={'features-ucc-app'} />
                        </div>
                    </Grid>
                    <Grid item xs={10} style={{
                        marginLeft: 20
                    }}>
                        <TextTitle size={26} color={colors.primary}>{props.title}</TextTitle>
                        <Space size={5} />
                        <TextContent color={colors.greyD}>{props.content}</TextContent>
                    </Grid>
                </div>
            ) : (
                    <div className={'flex-row-center'} style={{
                        margin: '20px 0px'
                    }}>
                        <Grid item xs={10} style={{
                            marginRight: 20
                        }}>
                            <TextTitle textAlign={'right'} size={26} color={colors.primary}>{props.title}</TextTitle>
                            <Space size={5} />
                            <TextContent textAlign={'right'} color={colors.greyD}>{props.content}</TextContent>
                        </Grid>
                        <Grid item xs={2}>
                            <div className={'logo-header-layout'}>
                                <img style={{
                                    width: 35,
                                    height: 35,
                                    objectFit: 'cover',
                                    margin: 0
                                }} src={props.icon === null ? images.imageNull : props.icon} 
                                alt={'features-ucc-app'} />
                            </div>
                        </Grid>
                    </div>
                )}
        </>
    )
}

export const StatisticCard = (props) => {

    return (
        <div style={{
            height: '40vw',
            maxHeight: 180,
            maxWidth: 180,
            width: '40vw',
            padding: 20,
            backgroundColor: colors.primary,
            borderRadius: 100
        }}>
            {props.icon}
            <div>
                <TextTitle center color={colors.light}>{props.title}</TextTitle>
                <Space />
                <TextContent center color={colors.light}>{props.content}</TextContent>
            </div>
        </div>
    )
}