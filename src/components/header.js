import React, { useState, useEffect } from "react";
import { navigate } from "@reach/router";
import IconButton from '@material-ui/core/IconButton';
import useMediaQuery from '@material-ui/core/useMediaQuery';
import { useTheme } from '@material-ui/core/styles';
import Drawer from '@material-ui/core/Drawer';
import { Link } from "gatsby";
import PropTypes from "prop-types";
import images from '../constants/images';
import { useTracked } from '../service';
import { MenuHeader, MenuDrawer, Space } from '../components/container';
import { menuName, initialSubPage } from "../constants";

import MenuOpenIcon from '@material-ui/icons/MenuOpen';
import ChevronRightIcon from '@material-ui/icons/ChevronRight';
import colors from "../constants/color";
import { TextContent } from "./text";
import { getStorage } from "../helpers/store";
import { decripter } from "../../server/helper/AES";
import { uccApiPrivate } from "../service/api";

const listMenu = [
  { id: menuName.home, name: 'home' },
  { id: menuName.features, name: 'features' },
  { id: menuName.download, name: 'download' },
  { id: menuName.contact, name: 'contact' },
  { id: menuName.sponsore, name: 'supported by' }
]

const Header = (props) => {
  const [state, action] = useTracked();
  const [open, setOpen] = useState(false);
  const [shadow, setShadow] = useState(true);

  const theme = useTheme();
  const hide = useMediaQuery(theme.breakpoints.down('sm'));

  useEffect(() => {
    if (typeof window !== `undefined`) {
      if (window.location.pathname !== '/') {
        setShadow(false)
      }
    }
  }, [])

  function scrollMenu(id) {
    const yOffset = -50;
    const element = document.getElementById(id);
    if (element !== null) {
      const y = element.getBoundingClientRect().top + window.pageYOffset + yOffset;
      window.scrollTo({ top: y, behavior: 'smooth' });
    }
    action({ type: 'setMenu', menu: id });
  }

  function navScroll(id) {
    if (typeof window !== `undefined`) {
      setOpen(false)
      if (window.location.pathname !== '/') {
        setTimeout(function () {
          navigate('/');
        }, 200);
        setTimeout(function () {
          scrollMenu(id)
        }, 1000);
      } else {
        scrollMenu(id)
      }
    }
  }

  return (
    <header
      className={'layout-header'}
      style={{
        boxShadow: shadow ? 'rgba(0, 0, 0, 0.2) 0px 1px 5px 0px' : 'none',
        borderBottom: shadow ? 'none' : '1px solid ' + colors.greyL
      }}
    >
      <div
        className={'layout-header-content'}
      >
        <Link className={'text-link'}
          to="/"
        >
          <div style={{
            padding: '0px 5px',
            backgroundColor: colors.light,
            borderRadius: 7
          }} className={'flex-row-left'}>
            <img src={images.logo} className={'img-logo'} alt={'universal campus consortium'} />
          </div>
        </Link>
        <div className={'layout-menu'}>
          {hide ? (
            <div>
              <IconButton
                onClick={() => {
                  setOpen(!open)
                }}>
                <MenuOpenIcon style={{
                  fontSize: 40,
                  color: colors.primary
                }} />
              </IconButton>
            </div>
          ) : listMenu.map((row, i) => (
            <div key={i.toString()}>
              <MenuHeader
                active={state.menu === row.id}
                title={row.name}
                onClick={() => {
                  navScroll(row.id)
                }}
              />
            </div>
          ))}
        </div>
        <Drawer
          anchor={'right'}
          open={open}
          onClose={() => {
            setOpen(false)
          }}>
          <div style={{
            width: '50vw',
            height: '100%',
            backgroundColor: colors.light,
          }}>
            <div>
              <IconButton
                onClick={() => {
                  setOpen(!open)
                }}>
                <ChevronRightIcon style={{
                  fontSize: 40,
                  color: colors.primary,
                }} />
              </IconButton>
            </div>
            <hr style={{
              height: 2,
              marginTop: 3,
              backgroundColor: colors.primary
            }} />
            <div>
              {
                listMenu.map((row, i) => (
                  <div key={i.toString()}>
                    <MenuDrawer
                      active={state.menu === row.id}
                      title={row.name}
                      onClick={() => {
                        navScroll(row.id)
                      }}
                    />
                  </div>
                ))
              }
            </div>
          </div>
        </Drawer>
      </div>
    </header>
  )
}

Header.propTypes = {
  siteTitle: PropTypes.string,
}

Header.defaultProps = {
  siteTitle: ``,
}

export default Header

export const PageHeader = (props) => {

  const [login, setLogin] = useState(false);
  const [subPage, setSubPage] = useState(initialSubPage)
  const [state, action] = useTracked();

  useEffect(() => {
    getLogin()
  }, [])

  async function getLogin() {
    const data = getStorage(props.payload.page);
    if (data !== null) {
      if (data.token !== undefined) {
        const user = decripter(data.token, data.key);
        const getAccess = await uccApiPrivate('enduserAccessMenu', user.username, data.token);
        if (Array.isArray(getAccess)) {
          const checkAccess = getAccess.filter(item => Number(item.accessMenuId) === props.payload.idmasterAdminAccess)
          // const checkAccess = getAccess.filter(item => Number(item.accessMenuId) === 12)
          if (checkAccess.length !== 0) {
            const subPageData = [...subPage]
            const newData = subPageData.filter(item => item.idlandingMenu !== 3 && item.idlandingMenu !== 4)
            setSubPage(newData)
            setLogin(true)
            action({
              type: 'successAlert',
              message: 'Welcome ' + user.username
            })
          }
        }
      }
    }
  }

  return (
    <div className={'layout-sub-header'}>
      <Space />
      <div className={'flex-row-left'}>
        {subPage.map((row, i) => (
          <div
            key={i}
            className={state.pageMenu.idlandingMenu === row.idlandingMenu ? 'button-sub-header-active' : 'button-sub-header'}
            onClick={() => {
              console.log(row)
              action({
                type: 'setPageMenu',
                data: row
              })
            }}
          >
            {
              i === 0 ? (
                <TextContent center title color={colors.light}>{props.payload.page}</TextContent>
              ) : (
                  <TextContent center title color={colors.light}>{row.landingPageMenuName}</TextContent>
                )
            }
          </div>
        ))}
        {
          login ? (
            <div
              className={'button-sub-header'}
              onClick={() => {
                navigate('/' + window.location.pathname.split('/')[1] + '/dashboard')
              }}
            >
              {
                <TextContent center title color={colors.light}>{'Dashboard ' + props.payload.page}</TextContent>
              }
            </div>
          ) : (
              null
            )
        }
      </div>
      <Space />
    </div>
  )
}

export const PageHeaderUccSiap = (props) => {

  const [login, setLogin] = useState(false);
  const [subPage, setSubPage] = useState(initialSubPage)
  const [state, action] = useTracked();

  // useEffect(() => {
    
  // }, [])

  return (
    <div className={'layout-sub-header'}>
      <Space />
      <div className={'flex-row-left'}>
        <div
          className={state.pageMenu.idlandingMenu === 1 ? 'button-sub-header-active' : 'button-sub-header'}
          onClick={() => {
            action({
              type: 'setPageMenu',
              data: {
                idlandingMenu: 1
              }
            })
          }}
        >
          {
            <TextContent center title color={colors.light}>{'UCC SIAP'}</TextContent>
          }
        </div>
        <div
          className={state.pageMenu.idlandingMenu === 2 ? 'button-sub-header-active' : 'button-sub-header'}
          onClick={() => {
            action({
              type: 'setPageMenu',
              data: {
                idlandingMenu: 2
              }
            })
          }}
        >
          {
            <TextContent center title color={colors.light}>{'blog'}</TextContent>
          }
        </div>
        <div
          className={'button-sub-header'}
          onClick={() => {
            window.location = "http://uccsiap.ucc-edu.com/login";
            // window.open("http://uccsiap.ucc-edu.com/login");
          }}
        >
          {
            <TextContent center title color={colors.light}>{'login'}</TextContent>
          }
        </div>
      </div>
      <Space />
    </div>
  )
}
