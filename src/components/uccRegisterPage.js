import React, { useState, useEffect } from "react";
import { navigate } from "@reach/router";
import { Button } from '@material-ui/core';
import { InputTextNormal, InputTextPassword } from "./form";
import { Space } from "./container";
import { useTracked } from "../service";
import { uccApiPublic, uccApiPrivate } from "../service/api";
import { setStorage, getStorage } from "../helpers/store";
import { generateToken } from "../helpers/AES";
import { decripter } from "../../server/helper/AES";
import moment from "moment";

export const MainRegister = (id) => {

    switch (id) {
        case 1:
            return {
                content: {
                    "campusName": "",
                    "campusAccreditation": "",
                    "address": "",
                    "district": "",
                    "subdistrict": "",
                    "city": "",
                    "province": "",
                    "country": "",
                    "docOpeningApprovalLetterCampus": "",
                    "docNotaryDeedofEstablishment": "",
                    "docRatificationCampus": "",
                    "campusFoto": ""
                },
                steper: [
                    {
                        title: 'Pendaftaran uccamp',
                    },
                    {
                        title: 'Kelengkapan data uccamp'
                    },
                    {
                        title: 'data pendukung pendaftaran uccamp'
                    }
                ],
                link: 'registerUccamp'
            }
        case 2:
            return {
                content: {
                    "campusName": "",
                    "campusAccreditation": "",
                    "address": "",
                    "district": "",
                    "subdistrict": "",
                    "city": "",
                    "province": "",
                    "country": "",
                    "docOpeningApprovalLetterCampus": "",
                    "docNotaryDeedofEstablishment": "",
                    "docRatificationCampus": ""
                },
                steper: [
                    {
                        title: 'Pendaftaran Akreditasi',
                    },
                    {
                        title: 'Kelengkapan data Akreditasi'
                    },
                    {
                        title: 'data pendukung pendaftaran Akreditasi'
                    }
                ],
                link: 'registerUcakreditasi'
            }
        case 3:
            return {
                content: {
                    "shopName": "",
                    "shopDescription": "",
                    "ownerName": "",
                    "kindOfShop": "",
                    "address": "",
                    "district": "",
                    "subdistrict": "",
                    "city": "",
                    "province": "",
                    "country": "",
                    "shopFoto": "",
                    "ownerFoto": ""
                },
                steper: [
                    {
                        title: 'Pendaftaran ucshop',
                    },
                    {
                        title: 'Kelengkapan data ucshop'
                    },
                    {
                        title: 'data pendukung pendaftaran ucshop'
                    }
                ],
                link: 'registerUcshop'
            }
        case 4:
            return {
                content: {
                    "employeeNo": "",
                    "nameWithTittle": "",
                    "birthDate": "2020-04-21",
                    "placeOfBirth": "",
                    "address": "",
                    "foto": "",
                    "citizenship": "",
                    "religion": "",
                    "lastEducation": "",
                    "yearsOfTeaching": "",
                    "gender": "",
                    "docCV": "",
                    "docCertificate": "",
                    "docTranscripts": "",
                    "docTeachingExperience": ""
                },
                steper: [
                    {
                        title: 'Pendaftaran Ucteach',
                    },
                    {
                        title: 'Kelengkapan data Ucteach'
                    },
                    {
                        title: 'data pendukung pendaftaran Ucteach'
                    }
                ],
                link: 'registerUcteach'
            }
        case 5:
            return {
                content: {
                    "companyName": "",
                    "companyDescription": "",
                    "ownerName": "",
                    "kindOfCompany": "",
                    "address": "",
                    "district": "",
                    "subdistrict": "",
                    "city": "",
                    "province": "",
                    "country": "",
                    "companyFoto": "",
                    "ownerFoto": ""
                },
                steper: [
                    {
                        title: 'Pendaftaran Ucjob',
                    },
                    {
                        title: 'Kelengkapan data Ucjob'
                    },
                    {
                        title: 'data pendukung pendaftaran Ucjob'
                    }
                ],
                link: 'registerUcjob'
            }
        case 6:
            return {
                content: {
                    "nameWithTittle": "",
                    "birthDate": "2020-04-21",
                    "placeOfBirth": "",
                    "address": "",
                    "foto": "",
                    "citizenship": "",
                    "religion": "",
                    "lastEducation": "",
                    "gender": "",
                    "docCV": "",
                    "docCertificate": "",
                    "docTranscripts": ""
                },
                steper: [
                    {
                        title: 'Pendaftaran Ucbiz',
                    },
                    {
                        title: 'Kelengkapan data Ucbiz'
                    },
                    {
                        title: 'data pendukung pendaftaran Ucbiz'
                    }
                ],
                link: 'registerUcbiz'
            }
        case 7:
            return {
                content: {
                    "campusOrCompany": "",
                    "nameWithTittle": "",
                    "birthDate": "2020-04-21",
                    "placeOfBirth": "",
                    "address": "",
                    "foto": "",
                    "citizenship": "",
                    "religion": "",
                    "lastEducation": "",
                    "gender": "",
                    "docCV": "",
                    "docCertificate": "",
                    "docTranscripts": ""
                },
                steper: [
                    {
                        title: 'Pendaftaran Uclearn',
                    },
                    {
                        title: 'Kelengkapan data Uclearn'
                    },
                    {
                        title: 'data pendukung pendaftaran Uclearn'
                    }
                ],
                link: 'registerUclearn'
            }
        case 8:
            return {
                content: {
                    "campusName": "",
                    "nameWithTittle": "",
                    "collegeStudentNo": "",
                    "birthDate": "2020-04-21",
                    "placeOfBirth": "",
                    "address": "",
                    "foto": "",
                    "citizenship": "",
                    "religion": "",
                    "lastEducation": "",
                    "yearsOfGraduation": "",
                    "yearsOfEntry": "",
                    "gender": "",
                    "docCV": "",
                    "docCertificate": "",
                    "docTranscripts": ""
                },
                steper: [
                    {
                        title: 'Pendaftaran Ucalumni',
                    },
                    {
                        title: 'Kelengkapan data Ucalumni'
                    },
                    {
                        title: 'data pendukung pendaftaran Ucalumni'
                    }
                ],
                link: 'registerUcalumni'
            }        
        default:
            throw ('component not found')
    }
}

const RegisterFeature = (props) => {

    const [state, action] = useTracked();

    const [payload, setPayload] = useState({
        username: '',
        password: '',
        name: '',
        email: '',
        telephone: ''
    })

    useEffect(() => {
        console.log(state.mainPage)
    }, [])

    function doError(message) {
        action({
            type: 'errorAlert',
            message
        })
    }

    function doSuccess(key, data) {
        setStorage(key, data)
        action({
            type: 'setStep',
            data: 1
        })
    }

    function loading(start) {
        if (start) {
            action({ type: 'loadStart' })
        } else {
            action({ type: 'loadStop' })
        }
    }

    async function doRegister() {
        loading(true)
        const data = await uccApiPublic('registerEnduser', payload)
        if (data) {
            loading(false)
            if (data === 'Register Success') {
                doLogin()
            } else {
                console.log(data)
                doError(data)
            }
        }
    }

    async function doLogin() {
        loading(true)
        const obj = {
            username: payload.username,
            password: payload.password
        }
        const data = await uccApiPublic('loginEnduser', obj)
        if (data) {
            loading(false)
            if (data.length > 100) {
                const token = await generateToken(obj)
                const newToken = {
                    token: token.token,
                    key: token.key,
                    jwt: data
                }
                doSuccess(state.mainPage.page, newToken)
                action({
                    type: 'successAlert'
                })
            } else {
                doError(data)
            }
        }
    }

    return (
        <div>
            <InputTextNormal
                value={payload.username}
                onChange={(e) => {
                    const data = { ...payload };
                    data.username = e.target.value
                    setPayload(data)
                }}
                name={'username'}
            />
            <InputTextPassword
                value={payload.password}
                onChange={(e) => {
                    const data = { ...payload };
                    data.password = e.target.value
                    setPayload(data)
                }}
                name={'password'}
            />
            <InputTextNormal
                value={payload.name}
                onChange={(e) => {
                    const data = { ...payload };
                    data.name = e.target.value
                    setPayload(data)
                }}
                name={'name'}
            />
            <InputTextNormal
                value={payload.email}
                onChange={(e) => {
                    const data = { ...payload };
                    data.email = e.target.value
                    setPayload(data)
                }}
                name={'email'}
            />
            <InputTextNormal
                value={payload.telephone}
                onChange={(e) => {
                    const data = { ...payload };
                    data.telephone = e.target.value
                    setPayload(data)
                }}
                name={'telephone'}
            />
            <Space size={50} />
            <Button onClick={() => {
                doRegister()
            }} size={'large'} variant={'contained'} color={'primary'}>Next</Button>
        </div>
    )
}

export const UCCReg = (props) => {

    const [state, action] = useTracked();
    const [payload, setPayload] = useState(props.payload)

    function doError(message) {
        action({
            type: 'errorAlert',
            message
        })
    }

    function doSuccess(key, data) {
        setStorage(key, data)
        action({
            type: 'setStep',
            data: 1
        })
    }

    function loading(start) {
        if (start) {
            action({ type: 'loadStart' })
        } else {
            action({ type: 'loadStop' })
        }
    }

    async function giveAccess() {
        loading(true)
        const getUser = getStorage(state.mainPage.page);
        const user = decripter(getUser.token, getUser.key);
        let data = {
            "username": user.username,
            "menuId": state.mainPage.idmasterAdminAccess.toString(),
            "status": "active",
            "createdBy": user.username,
            "updatedBy": user.username,
            "createdDate": moment().format('YYYY-MM-DD HH:mm:ss'),
            "expiredDate": moment().add(1, 'years').format('YYYY-MM-DD HH:mm:ss'),
            "updateDate": moment().format('YYYY-MM-DD HH:mm:ss')
        };
        const req = await uccApiPrivate('giveUserAccessToMenu', data, getUser.token)
        if (req) {
            loading(false)
            console.log(req)
            window.location.reload();
        }
    }

    async function doRegister() {
        loading(true)
        const getUser = getStorage(state.mainPage.page);
        const user = decripter(getUser.token, getUser.key);
        let data = payload;
        data.username = user.username
        const req = await uccApiPrivate(props.link, data, getUser.token)
        if (req) {
            loading(false)
            console.log(req)
            if (req === 'Register Success') {
                await giveAccess()
            } else {
                console.log(req)
                doError(req)
            }
        }
    }

    function generateState(flag) {
        let keys = Object.keys(props.payload);
        if (flag) {
            return keys.slice(0, 7)
        } else {
            return keys.slice(7, keys.length)
        }
    }

    function getName(row) {
        const data = row.split(/(?=[A-Z])/);
        let name = ''
        for (let i in data) {
            if (i === '0') {
                name = name + data[i]
            } else {
                name = name + ' ' + data[i]
            }
        }
        return name
    }

    // useEffect(() => {
    // }, [])

    if (state.step === 2) {
        return (
            <div>
                {
                    generateState(false).map((row, i) => (
                        <div key={i} style={{
                            marginBottom: 25
                        }}>
                            <InputTextNormal
                                value={payload[row]}
                                onChange={(e) => {
                                    const data = { ...payload };
                                    data[row] = e.target.value
                                    setPayload(data)
                                }}
                                name={getName(row)}
                            />
                        </div>
                    ))
                }
                <Space size={50} />
                <Button onClick={() => {
                    console.log(payload)
                    action({
                        type: 'setStep',
                        data: 1
                    })
                }} size={'large'} variant={'contained'} color={'primary'}>Back</Button>
                <Button
                    style={{
                        marginLeft: 10
                    }}
                    onClick={() => {
                        doRegister()
                    }} size={'large'} variant={'contained'} color={'primary'}>Finish</Button>
            </div>
        )
    } else if (state.step === 1) {
        return (
            <div>
                {
                    generateState(true).map((row, i) => (
                        <div key={i} style={{
                            marginBottom: 25
                        }}>
                            <InputTextNormal
                                value={payload[row]}
                                onChange={(e) => {
                                    const data = { ...payload };
                                    data[row] = e.target.value
                                    setPayload(data)
                                }}
                                name={getName(row)}
                            />
                        </div>
                    ))
                }
                <Space size={50} />
                <Button onClick={() => {
                    console.log(payload)
                    action({
                        type: 'setStep',
                        data: 2
                    })
                }} size={'large'} variant={'contained'} color={'primary'}>Next</Button>
            </div>
        )
    } else {
        return (
            <RegisterFeature />
        )
    }
}