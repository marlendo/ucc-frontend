
import React, { useEffect, useState } from "react";
import { HeaderChart } from '../../cart';
import { chartYears } from "../../../helpers/parse";
import colors from '../../../constants/color';
import { Space } from '../../container';

const data = (canvas) => {

  return {
    backgroundColor: '#000',

  }
}

const Dashboard = () => {

  const [login, setLogin] = useState(false);

  //   function getLogin() {
  //     const login = getStorage('login');
  //     if (login !== null) {
  //       if (login.token !== undefined) {
  //         setLogin(true)
  //       }
  //     }
  //   }

  //   useEffect(() => {
  //     getLogin()
  //   }, [])

  return (
    <div>
      <Space size={10} />
      <div style={{
        width: '100%',
        flexWrap: 'wrap'
      }} className={'flex-row-center'}>
        <HeaderChart
          label={'new Users'}
          color={colors.fourdaryD}
          payload={chartYears([0, 59, 80, 81, 56, 55, 40])}
        />
        <HeaderChart 
        label={'university Joined'}
        color={colors.secondary} 
        payload={chartYears([0, 2, 1, 3, 5])} 
        />
        <HeaderChart 
        label={'Dosen Contribution'}
        color={colors.thridary} 
        payload={chartYears([0, 10, 1, 1, 4, 6, 1, 1, 2, 3, 4, 5, 1])} 
        />
      </div>
    </div>
  )
}

export default Dashboard