
import React, { useEffect, useState } from "react";
import { useTracked as MainTracked } from '../../../service';
import { Profile, Slider, Menu, About, Feature } from './landingPage';
import { getLanding } from "../../../service/api";

const Landing = (props) => {

  const [state, action] = MainTracked()
  const [render, setRender] = useState(false)

  async function getData() {
    setTimeout(function () {
      setRender(true)
      action({
        type: 'setSubMenu',
        subMenu: props.menu[0]
      })
    }, 200);
  }

  useEffect(() => {
    getData()
  }, [])

  if (render) {
    switch (state.subMenu.title) {
      case props.menu[0].title:
        return (
          <Profile />
        )
      case props.menu[1].title:
        return (
          <Slider />
        )
      case props.menu[2].title:
        return (
          <Menu />
        )
      case props.menu[3].title:
        return (
          <Feature />
        )
      case props.menu[4].title:
        return (
          <About />
        )
      default:
        return (
          null
        )
    }
  } else {
    return (
      null
    )
  }
}

export default Landing