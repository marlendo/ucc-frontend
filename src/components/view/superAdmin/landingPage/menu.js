import React, { useEffect, useState } from "react";
import { Grid, Button, IconButton } from '@material-ui/core';
import { useTracked } from '../../../../service';
import colors from '../../../../constants/color';
import { TextTitle, TextContent } from "../../../text";
import { InputTextNormal } from "../../../form";
import { Space } from "../../../container";

import CheckCircleIcon from '@material-ui/icons/CheckCircle';
import ChevronRightIcon from '@material-ui/icons/ChevronRight';
import ChevronLeftIcon from '@material-ui/icons/ChevronLeft';

import { LeaderCard } from "../../../card";
import { uccApi, adminApi } from '../../../../service/api';
import { mover } from "../../../../helpers/parse";

const Menu = (props) => {

    const [state, action] = useTracked();
    const [payload, setPayload] = useState([]);
    const [master, setMaster] = useState(false);
    const [editor, setEditor] = useState(false)

    async function setData() {
        try {
            action({
                type: 'loadStart'
            })
            const request = await adminApi({
                method: 'get',
                url: 'landing-menu',
                data: {}
            })
            if (request) {
                if (!request.error) {
                    setPayload(request.data)
                    getMaster(request.data)
                } else {
                    action({
                        type: 'errorAlert',
                    })
                }
            }
        } catch (error) {
            console.log(error)
        }
    }

    async function getMaster(menu) {
        let data = await uccApi({
            method: "post",
            url: "masterAdminAccess",
            data: {}
        })
        if (data) {
            for (let i in data) {
                for (let j in menu) {
                    if (data[i].idmasterAdminAccess === menu[j].idmasterAdminAccess && !menu[j].hidden) {
                        data[i].checked = true
                    }
                }
            }
            setMaster(data)
            action({
                type: 'loadStop'
            })
        }
    }

    async function updateData(data, i) {
        try {
            const update = await adminApi({
                method: 'put',
                url: 'landing-menu',
                data: {
                    id: data.id,
                    img: 'menu' + i === state.name ? state.image === '' ? null : state.image : data.img,
                    title: data.title,
                    order: data.order,
                    hidden: data.hidden
                }
            })
            if (Number(i) === (payload.length - 1)) {
                if (update) {
                    action({
                        type: 'loadStop'
                    })
                    if (!update.error) {
                        action({
                            type: 'successAlert',
                        })
                    } else {
                        action({
                            type: 'errorAlert',
                        })
                    }
                }
            }
        } catch (error) {
            console.log(error)
        }
    }

    async function saveData(data, i) {
        try {
            const update = await adminApi({
                method: 'post',
                url: 'landing-menu',
                data: {
                    idmasterAdminAccess: data.idmasterAdminAccess,
                    img: 'menu' + i === state.name ? state.image === '' ? null : state.image : data.img,
                    title: data.title,
                    order: data.order,
                    hidden: false
                }
            })
            if (Number(i) === (payload.length - 1)) {
                if (update) {
                    action({
                        type: 'loadStop'
                    })
                    if (!update.error) {
                        action({
                            type: 'successAlert',
                        })
                    } else {
                        action({
                            type: 'errorAlert',
                        })
                    }
                }
            }
        } catch (error) {
            console.log(error)
        }
    }

    function doStore() {
        action({
            type: 'loadStart'
        })
        for (let i in payload) {
            const data = payload[i];
            if (data.id) {
                updateData(data, i)
            } else {
                saveData(data, i)
            }
        }
    }

    function openEditor(row, i) {
        setEditor(i)
    }


    useEffect(() => {
        setData()
    }, [])

    return (
        <Grid container
            direction="row">
            <Grid item md={12} style={{
                backgroundColor: colors.greyL,
                margin: 10,
                padding: 20
            }}>
                <div style={{
                    padding: 20
                }}>
                    <TextTitle>Master Menu</TextTitle>
                </div>
                <div style={{
                    flexWrap: 'wrap'
                }} className={'flex-row-left'}>
                    {
                        master ?
                            master.map((row, i) => (
                                <div style={{
                                    display: 'flex',
                                    margin: '20px 10px',
                                    position: 'relative'
                                }} key={i}
                                    onClick={() => {
                                        if (row.idmasterAdminAccess === 10) {
                                            alert('Super Admin Cannot added to public menu')
                                        } else {
                                            const spy = payload.filter(item => item.idmasterAdminAccess === row.idmasterAdminAccess)
                                            if (spy.length === 0 || spy[0].hidden) {
                                                if (spy.length === 0) {
                                                    const data = [...payload, {
                                                        id: false,
                                                        idmasterAdminAccess: row.idmasterAdminAccess,
                                                        img: null,
                                                        title: row.AdminAccessName,
                                                        order: (payload.length)
                                                    }];
                                                    setPayload(data)
                                                    const update = [...master];
                                                    update[i].checked = true;
                                                    setMaster(update)
                                                } else {
                                                    const field = payload.filter(item => item.idmasterAdminAccess === row.idmasterAdminAccess)[0];
                                                    const index = payload.indexOf(field);
                                                    const newData = [...payload];
                                                    newData[index].hidden = false;
                                                    setPayload(newData)
                                                    const update = [...master];
                                                    update[i].checked = true;
                                                    setMaster(update)
                                                }
                                            } else {
                                                    const field = payload.filter(item => item.idmasterAdminAccess === row.idmasterAdminAccess)[0];
                                                    const index = payload.indexOf(field);
                                                    const newData = [...payload];
                                                    newData[index].hidden = true;
                                                    setPayload(newData)
                                                    const update = [...master];
                                                    update[i].checked = false;
                                                    setMaster(update)
                                            }
                                        }
                                    }}
                                >
                                    <LeaderCard
                                        active={row.checked}
                                        size={70}
                                        image={
                                            null
                                        }
                                        title={row.AdminAccessName}
                                    />
                                    {
                                        row.checked ? (
                                            <div style={{
                                                position: 'absolute',
                                                top: 0,
                                                left: 0,
                                            }}>
                                                <CheckCircleIcon style={{
                                                    fontSize: 40,
                                                    color: colors.danger
                                                }} />
                                            </div>
                                        ) : (
                                                null
                                            )
                                    }
                                </div>
                            )) : null
                    }
                </div>
                <Space size={30} />
            </Grid>
            <Grid item md={12} className={'flex-column-center'} style={{
                backgroundColor: colors.greyL,
                margin: 10,
                width: '100%',
                padding: 20
            }}>
                <div style={{
                    padding: 20,
                    alignSelf: 'flex-start'
                }}>
                    <TextTitle>Active Page Menu</TextTitle>
                </div>
                <Space size={50} />
                <div style={{
                    flexWrap: 'wrap'
                }} className={'flex-row-left'}>
                    {payload.map((row, i) => (
                        row.hidden ? (null) : (
                            <div key={i} className={'flex-column-center'}>
                                <div style={{
                                    display: 'flex',
                                    margin: 10,
                                }} onClick={() => {
                                    openEditor(row, i)
                                }}>
                                    <LeaderCard
                                        size={80}
                                        image={row.img}
                                        title={row.title}
                                    />
                                </div>
                                <div style={{
                                    padding: 10,
                                    width: 'calc(100% - 20px)',
                                    backgroundColor: colors.greyD,
                                    marginTop: 5
                                }} className={'flex-row-space'}>
                                    {
                                        i === 0 ? (
                                            <div />
                                        ) : (
                                                <div
                                                    className={'flex-row-center button-small-page'}
                                                    onClick={() => {
                                                        let data = [...payload];
                                                        data[i].order = i - 1;
                                                        data[i - 1].order = i + 1;
                                                        let move = mover(data, i, (i - 1));
                                                        console.log(move)
                                                        setPayload(move);
                                                    }}
                                                >
                                                    <ChevronLeftIcon />
                                                </div>
                                            )
                                    }
                                    <TextContent color={colors.light}>Page {Number(i) + 1}</TextContent>
                                    {
                                        Number(i) === (Number(payload.length) - 1) ? (
                                            <div />
                                        ) : (
                                                <div
                                                    onClick={() => {
                                                        let data = [...payload];
                                                        data[i].order = i + 1;
                                                        data[i + 1].order = i - 1;
                                                        let move = mover(data, i, (i + 1));
                                                        console.log(move)
                                                        setPayload(move);
                                                    }}
                                                    className={'flex-row-center button-small-page'}>
                                                    <ChevronRightIcon />
                                                </div>
                                            )
                                    }
                                </div>
                                {
                                    editor !== false && editor === i ? (
                                        <div className={'editor'}>
                                            <div style={{
                                                pading: 20
                                            }}>
                                                <TextTitle color={colors.light} center>Update Menu</TextTitle>
                                            </div>
                                            <InputTextNormal
                                                value={payload[editor].title}
                                                onChange={null}
                                                color={colors.light}
                                                disabled
                                                error={false}
                                                name={'title ' + Number(i + 1)}
                                            />
                                            <div style={{
                                                padding: 10
                                            }}>
                                                <TextContent color={colors.light}>Image</TextContent>
                                            </div>
                                            <div
                                                onClick={() => {
                                                    action({
                                                        type: 'setDopen',
                                                        name: 'menu' + i
                                                    })
                                                }} className={'flex-row-center'}>
                                                {state.name === 'menu' + i && state.image !== '' ? (
                                                    <img
                                                        style={{
                                                            width: 100,
                                                            height: 100,
                                                            margin: 0
                                                        }}
                                                        src={state.image}
                                                        alt={'props.image'}
                                                    />
                                                ) : (
                                                        row.img === null ? (
                                                            <div style={{
                                                                width: 100,
                                                                height: 100,
                                                                backgroundColor: colors.fourdary,
                                                                cursor: 'pointer'
                                                            }} className={'flex-row-center'}>
                                                                <TextContent title color={colors.light}>
                                                                    No Image Found
                                                    </TextContent>
                                                            </div>
                                                        ) : (
                                                                <img
                                                                    style={{
                                                                        width: 100,
                                                                        height: 100,
                                                                        margin: 0
                                                                    }}
                                                                    src={row.img}
                                                                    alt={'props.image'}
                                                                />
                                                            )
                                                    )}
                                            </div>
                                            <div className={'flex-row-center'} style={{
                                                margin: '40px 0 0 0'
                                            }}>
                                                <Button
                                                    onClick={() => {
                                                        setEditor(false)
                                                    }}
                                                    variant="contained" size={'small'} color="primary">
                                                    cancel
                                        </Button>
                                                <div style={{
                                                    width: 20
                                                }} />
                                                <Button
                                                    onClick={() => {
                                                        let data = [...payload];
                                                        data[i].img = state.image
                                                        setPayload(data);
                                                        setEditor(false)
                                                    }}
                                                    variant="contained" size={'small'} color="primary">
                                                    Save
                                        </Button>
                                            </div>
                                        </div>
                                    ) : (
                                            null
                                        )
                                }
                            </div>
                        )
                    ))}
                </div>
                <Space size={100} />
                <Button
                    onClick={() => {
                        doStore()
                    }}
                    style={{
                        width: 'calc(100% - 20vw)'
                    }} variant="contained" size={'large'} color="primary">
                    Save
                </Button>
                <Space size={30} />
            </Grid>
            <Space size={50} />
        </Grid>
    )
}

export default Menu