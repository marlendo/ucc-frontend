import React, { useEffect, useState } from "react";
import { Grid, Button, IconButton } from '@material-ui/core';
import { useTracked } from '../../../../service';
import colors from '../../../../constants/color';
import { InputTextNormal, AddressComponent } from "../../../form";
import { Space } from "../../../container";

import { adminApi } from '../../../../service/api';

const Profile = (props) => {

    const [state, action] = useTracked()
    const [payload, setPayload] = useState({
        address: '',
        phone: [''],
        email: [''],
        fb: '',
        ig: '',
        twet: ''
    })

    async function setData() {
        try {
            action({
                type: 'loadStart'
            })
            const request = await adminApi({
                method: 'get',
                url: 'landing-profile',
                data: {}
            })
            if (request) {
                action({
                    type: 'loadStop'
                })
                if (!request.error) {
                    setPayload(request.data)
                } else {
                    action({
                        type: 'errorAlert',
                    })
                }
            }
        } catch (error) {
            console.log(error)
        }
    }

    async function doSave() {
        try {
            if (window.confirm("save data ?")) {
                action({
                    type: 'loadStart'
                })
                const request = await adminApi({
                    method: 'put',
                    url: 'landing-profile',
                    data: JSON.stringify(payload)
                })
                if (request) {
                    action({
                        type: 'loadStop'
                    })
                    if (!request.error) {
                        action({
                            type: 'successAlert',
                        })
                        setPayload(request.data.payload)
                    } else {
                        action({
                            type: 'errorAlert',
                        })
                    }
                }
            }
        } catch (error) {
            console.log(error)
        }
    }

    useEffect(() => {
        setData()
    }, [])

    return (
        <Grid container
            direction="row">
            <Grid item md sm={12} className={'flex-column-start'} style={{
                backgroundColor: colors.greyL,
                margin: 10,
                padding: 20,
                width: '100%'
            }}>
                <InputTextNormal
                    value={payload.address}
                    onChange={(e) => {
                        const data = { ...payload };
                        data.address = e.target.value;
                        setPayload(data)
                    }}
                    error={false}
                    name={'address'}
                />
                {payload.phone.map((row, i) => (
                    <div key={i} style={{
                        width: '100%'
                    }}>
                        <InputTextNormal
                            value={row}
                            onChange={(e) => {
                                const data = { ...payload };
                                data.phone[i] = e.target.value;
                                setPayload(data)
                            }}
                            error={false}
                            name={'phone ' + (Number(i) + 1)}
                        />
                    </div>
                ))}
                {payload.email.map((row, i) => (
                    <div key={i} style={{
                        width: '100%'
                    }}>
                        <InputTextNormal
                            value={row}
                            onChange={(e) => {
                                const data = { ...payload };
                                data.email[i] = e.target.value;
                                setPayload(data)
                            }}
                            error={false}
                            name={'email ' + (Number(i) + 1)}
                        />
                    </div>
                ))}
                <InputTextNormal
                    value={payload.fb}
                    onChange={(e) => {
                        const data = { ...payload };
                        data.fb = e.target.value;
                        setPayload(data)
                    }}
                    error={false}
                    name={'facebook'}
                />
                <InputTextNormal
                    value={payload.ig}
                    onChange={(e) => {
                        const data = { ...payload };
                        data.ig = e.target.value;
                        setPayload(data)
                    }}
                    error={false}
                    name={'instagram'}
                />
                <InputTextNormal
                    value={payload.twet}
                    onChange={(e) => {
                        const data = { ...payload };
                        data.twet = e.target.value;
                        setPayload(data)
                    }}
                    error={false}
                    name={'twitter'}
                />
            </Grid>
            <Grid item md sm={12} className={'flex-column-center'} style={{
                backgroundColor: colors.greyL,
                margin: 10
            }}>
                <div>
                    <AddressComponent payload={payload} />
                </div>
                <Button onClick={() => {
                    doSave()
                }} style={{
                    width: 'calc(100% - 20vw)'
                }} variant="contained" size={'large'} color="primary">
                    Save
                </Button>
                <Space size={30} />
            </Grid>
        </Grid>
    )
}

export default Profile