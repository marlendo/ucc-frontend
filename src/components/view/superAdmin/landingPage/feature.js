import React, { useEffect, useState } from "react";
import { Grid, Button } from '@material-ui/core';
import { useTracked } from '../../../../service';
import colors from '../../../../constants/color';
import { TextTitle } from "../../../text";
import { InputTextNormal } from "../../../form";
import { Space, AboutCompoment, FeatureComponent } from "../../../container";

import PermMediaIcon from '@material-ui/icons/PermMedia';
import { adminApi } from '../../../../service/api';


const Feature = (props) => {

    const [state, action] = useTracked()
    const [payload, setPayload] = useState({
        img: null,
        left: [
            {
                title: '',
                content: '',
                icon: null
            }
        ],
        right: [
            {
                title: '',
                content: '',
                icon: null
            }
        ]
    })

    async function setData() {
        try {
            action({
                type: 'loadStart'
            })
            const request = await adminApi({
                method: 'get',
                url: 'landing-feature',
                data: {}
            })
            if (request) {
                action({
                    type: 'loadStop'
                })
                if (!request.error) {
                    setPayload(request.data)
                } else {
                    action({
                        type: 'errorAlert',
                    })
                }
            }
        } catch (error) {
            console.log(error)
        }
    }

    async function doStore(data, i) {
        try {
            action({
                type: 'loadStart'
            })
            const update = await adminApi({
                method: 'put',
                url: 'landing-feature',
                data: {
                    img: 'feature' === state.name ? state.image === '' ? null : state.image : payload.img,
                    left: JSON.stringify(payload.left),
                    right: JSON.stringify(payload.right)
                }
            })
            if (update) {
                action({
                    type: 'loadStop'
                })
                if (!update.error) {
                    action({
                        type: 'successAlert',
                    })
                    setPayload(update.data.payload)
                } else {
                    action({
                        type: 'errorAlert',
                    })
                }
            }
        } catch (error) {
            console.log(error)
        }
    }


    useEffect(() => {
        setData()
    }, [])

    return (
        <Grid container
            direction="row">
            <Grid item sm={12} className={'flex-column-center'} style={{
                backgroundColor: colors.greyL,
                margin: 10,
            }}>
                <div style={{
                    padding: 50
                }}>
                    <FeatureComponent payload={payload} overide={{
                        img: state.image,
                        id: state.name
                    }} />
                </div>
            </Grid>
            <Grid container
                direction="row"
                justify="center"
                alignItems="center">
                <Grid item md className={'flex-column-start'} style={{
                    backgroundColor: colors.greyL,
                    margin: 10,
                    padding: 20,
                    width: '100%'
                }}>
                    <TextTitle>Left Content</TextTitle>
                    <Space size={30} />
                    {payload.left.map((row, i) => (
                        <div key={i} style={{
                            width: '100%'
                        }}>
                            <InputTextNormal
                                value={row.title}
                                onChange={(e) => {
                                    const data = { ...payload };
                                    data.left[i].title = e.target.value;
                                    setPayload(data)
                                }}
                                error={false}
                                name={'title ' + (Number(i) + 1)}
                            />
                            <div style={{
                                marginLeft: 20
                            }}>
                                <InputTextNormal
                                    size={10}
                                    small
                                    value={row.content}
                                    onChange={(e) => {
                                        const data = { ...payload };
                                        data.left[i].content = e.target.value;
                                        setPayload(data)
                                    }}
                                    error={false}
                                    name={'sub-title ' + (Number(i) + 1)}
                                />
                            </div>
                        </div>
                    ))}
                    <Space size={100} />
                    <div className={'flex-row-center'} style={{
                        width: '100%'
                    }}>
                    </div>
                </Grid>
                <Grid item md className={'flex-column-start'} style={{
                    backgroundColor: colors.greyL,
                    margin: 10,
                    padding: 20,
                    width: '100%'
                }}>
                    <TextTitle>Right Content</TextTitle>
                    <Space size={30} />
                    {payload.right.map((row, i) => (
                        <div key={i} style={{
                            width: '100%'
                        }}>
                            <InputTextNormal
                                value={row.title}
                                onChange={(e) => {
                                    const data = { ...payload };
                                    data.right[i].title = e.target.value;
                                    setPayload(data)
                                }}
                                error={false}
                                name={'title ' + (Number(i) + 1)}
                            />
                            <div style={{
                                marginLeft: 20
                            }}>
                                <InputTextNormal
                                    size={10}
                                    small
                                    value={row.content}
                                    onChange={(e) => {
                                        const data = { ...payload };
                                        data.right[i].content = e.target.value;
                                        setPayload(data)
                                    }}
                                    error={false}
                                    name={'sub-title ' + (Number(i) + 1)}
                                />
                            </div>
                        </div>
                    ))}
                    <Space size={100} />
                    <div className={'flex-row-center'} style={{
                        width: '100%'
                    }}>
                    </div>
                </Grid>
            </Grid>
            <div style={{
                width: '100%',
                margin: 100
            }} className={'flex-row-center'}>
                <div style={{
                    width: '30%',
                    marginRight: 20
                }} onClick={() => {
                    action({
                        type: 'setDopen',
                        name: 'feature'
                    })
                }}>
                    <div style={{
                        cursor: 'pointer',
                        padding: 20,
                        backgroundColor: colors.secondary,
                        borderRadius: 7
                    }} className={'flex-row-center'}>
                        <PermMediaIcon style={{
                            color: colors.light
                        }} />
                        <div style={{
                            marginLeft: 10
                        }}>
                            <TextTitle center color={colors.light}>Change Image</TextTitle>
                        </div>
                    </div>
                </div>
                <Button style={{
                    width: '70%',
                }}
                    variant="contained"
                    size={'large'}
                    color="primary"
                    onClick={() => {
                        doStore()
                    }}
                >
                    <TextTitle color={colors.light}>Save</TextTitle>
                </Button>
            </div>
        </Grid>
    )
}

export default Feature

