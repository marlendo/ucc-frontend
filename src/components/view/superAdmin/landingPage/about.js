import React, { useEffect, useState } from "react";
import { Grid, Button, IconButton } from '@material-ui/core';
import { useTracked } from '../../../../service';
import colors from '../../../../constants/color';
import { TextTitle } from "../../../text";
import { InputTextNormal } from "../../../form";
import { Space, AboutCompoment } from "../../../container";

import PermMediaIcon from '@material-ui/icons/PermMedia';

import { adminApi } from '../../../../service/api';

const About = (props) => {

    const [state, action] = useTracked()
    const [payload, setPayload] = useState({
        img: null,
        content: ['']
    })

    async function doStore() {
        const data = payload
        try {
            action({
                type: 'loadStart'
            })
            const update = await adminApi({
                method: 'put',
                url: 'landing-about',
                data: {
                    img: 'about' === state.name ? state.image === '' ? null : state.image : data.img,
                    content: JSON.stringify(payload.content)
                }
            })
            if (update) {
                action({
                    type: 'loadStop'
                })
                if (!update.error) {
                    action({
                        type: 'successAlert',
                    })
                    setPayload(update.data.payload)
                } else {
                    action({
                        type: 'errorAlert',
                    })
                }
            }
        } catch (error) {
            console.log(error)
        }
    }

    async function setData() {
        try {
            action({
                type: 'loadStart'
            })
            const request = await adminApi({
                method: 'get',
                url: 'landing-about',
                data: {}
            })
            if (request) {
                action({
                    type: 'loadStop'
                })
                if (!request.error) {
                    setPayload(request.data)
                } else {
                    action({
                        type: 'errorAlert',
                    })
                }
            }
        } catch (error) {
            console.log(error)
        }
    }


    useEffect(() => {
        setData()
    }, [])

    return (
        <Grid container
            direction="row">
            <Grid item md sm={12} className={'flex-column-center'} style={{
                backgroundColor: colors.greyL,
                margin: 10,
            }}>
                <div style={{
                    padding: '100px 20px'
                }}>
                    <AboutCompoment payload={payload} overide={{
                        img: state.image,
                        id: state.name
                    }} />
                </div>
            </Grid>
            <Grid item md sm={12} className={'flex-column-start'} style={{
                backgroundColor: colors.greyL,
                margin: 10,
                padding: 20,
                width: '100%'
            }}>
                {payload.content.map((row, i) => (
                    <div key={i} style={{
                        width: '100%'
                    }}>
                        <InputTextNormal
                            value={row}
                            onChange={(e) => {
                                const data = { ...payload };
                                data.content[i] = e.target.value;
                                setPayload(data)
                            }}
                            error={false}
                            name={'about content ' + (Number(i) + 1)}
                        />
                    </div>
                ))}
                <Space size={100} />
                <div className={'flex-row-center'} style={{
                    width: '100%'
                }}>
                    <div style={{
                        width: '30%',
                        marginRight: 20
                    }} onClick={() => {
                        action({
                            type: 'setDopen',
                            name: 'about'
                        })
                    }}>
                        <div style={{
                            cursor: 'pointer',
                            padding: 20,
                            backgroundColor: colors.secondary,
                            borderRadius: 7
                        }} className={'flex-row-center'}>
                            <PermMediaIcon style={{
                                color: colors.light
                            }} />
                            <div style={{
                                marginLeft: 10
                            }}>
                                <TextTitle center color={colors.light}>Change Image</TextTitle>
                            </div>
                        </div>
                    </div>
                    <Button
                        onClick={() => {
                            doStore()
                        }}
                        style={{
                            width: '100%'
                        }} variant="contained" size={'large'} color="primary">
                        <TextTitle color={colors.light}>Save</TextTitle>
                    </Button>
                </div>
            </Grid>
        </Grid>
    )
}

export default About