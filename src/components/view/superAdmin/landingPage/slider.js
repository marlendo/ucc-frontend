import React, { useEffect, useState } from "react";
import useMediaQuery from '@material-ui/core/useMediaQuery';
import { useTheme } from '@material-ui/core/styles';
import { Grid, Button, IconButton } from '@material-ui/core';
import { useTracked } from '../../../../service';
import colors from '../../../../constants/color';
import { TextTitle, TextContent } from "../../../text";
import { InputTextNormal } from "../../../form";
import { Space, UCCMenu, AboutCompoment, FeatureComponent } from "../../../container";
import { Slider as SliderComponent } from "../../../animation";

import AddCircleIcon from '@material-ui/icons/AddCircle';
import CheckCircleIcon from '@material-ui/icons/CheckCircle';
import PermMediaIcon from '@material-ui/icons/PermMedia';
import SaveIcon from '@material-ui/icons/Save';
import ChevronRightIcon from '@material-ui/icons/ChevronRight';
import ChevronLeftIcon from '@material-ui/icons/ChevronLeft';

import { LeaderCard } from "../../../card";
import { uccApi, adminApi } from '../../../../service/api';
import { mover } from "../../../../helpers/parse";

const Slider = (props) => {

    const [state, action] = useTracked()

    const theme = useTheme();
    const hide = useMediaQuery(theme.breakpoints.down('sm'));

    const initialState = {
        img: null,
        text: {
            title: 'Title Slider',
            content: [
                'Content Slider'
            ]
        },
        save: false
    }

    const [payload, setPayload] = useState([
        initialState
    ])


    function addSlider() {
        console.log(state)
        if (payload.length > 4) {
            alert('maximum slider')
        } else {
            setPayload([...payload, initialState])
        }
    }

    async function updateSlider(data) {
        try {
            const update = await adminApi({
                method: 'put',
                url: 'landing-slider',
                data: {
                    id: data.id,
                    img: state.image === '' ? null : state.image,
                    title: data.text.title,
                    content: JSON.stringify(data.text.content)
                }
            })
            action({
                type: 'loadStop'
            })
            if (!update.error) {
                action({
                    type: 'successAlert',
                })
                setPayload(update.data.payload)
            } else {
                action({
                    type: 'errorAlert',
                })
            }
        } catch (error) {
            console.log(error)
        }
    }

    async function saveSlider(data) {
        try {
            const update = await adminApi({
                method: 'post',
                url: 'landing-slider',
                data: {
                    img: state.image === '' ? null : state.image,
                    title: data.text.title,
                    content: JSON.stringify(data.text.content)
                }
            })
            action({
                type: 'loadStop'
            })
            if (!update.error) {
                action({
                    type: 'successAlert',
                })
                setPayload(update.data.payload)
            } else {
                action({
                    type: 'errorAlert',
                })
            }
        } catch (error) {
            console.log(error)
        }
    }

    function doStore(data) {
        if (window.confirm("save data ?")) {
            action({
                type: 'loadStart'
            })
            if (data.id) {
                updateSlider(data)
            } else {
                saveSlider(data)
            }
        }
    }

    async function setData() {
        try {
            action({
                type: 'loadStart'
            })
            const request = await adminApi({
                method: 'get',
                url: 'landing-slider',
                data: {}
            })
            if (request) {
                action({
                    type: 'loadStop'
                })
                if (!request.error) {
                    setPayload(request.data)
                } else {
                    action({
                        type: 'errorAlert',
                    })
                }
            }
        } catch (error) {
            console.log(error)
        }
    }

    useEffect(() => {
        setData()
    }, [])

    return (
        <Grid container
            direction="row"
            justify="center"
            alignItems="center">
            <Grid item xs={12} style={{
                backgroundColor: colors.greyL,
            }}>
                {hide ? (
                    <TextTitle center>Unable Preview On Small Screen</TextTitle>
                ) : (
                        <SliderComponent payload={payload} />
                    )}
            </Grid>
            <Space size={30} />
            <Grid item xs={12} style={{
                backgroundColor: colors.greyL,
                padding: 20
            }}>
                {
                    payload.map((row, i) => (
                        <div key={i}>
                            <div key={i} style={{
                                backgroundColor: colors.secondary,
                                padding: 30,
                                boxShadow: '0 0 10px 0 #000'
                            }}>
                                {
                                    row.save ? (
                                        <div style={{
                                            width: 300,
                                            height: 50,
                                            border: '2px dashed #FFFFFF'
                                        }} className={'flex-row-center'}>
                                            <TextTitle color={colors.light}>Unsaved</TextTitle>
                                        </div>
                                    ) : (
                                            null
                                        )
                                }
                                <Grid container
                                    direction="row"
                                    justify="center"
                                    alignItems="center">
                                    <Grid item xs={8} style={{
                                        padding: 20
                                    }}>
                                        <TextTitle color={colors.light}>Slide {Number(i) + 1}</TextTitle>
                                        <div style={{
                                            marginTop: 50,
                                        }}>
                                            <InputTextNormal
                                                color={colors.light}
                                                value={row.text.title}
                                                onChange={(e) => {
                                                    const data = [...payload];
                                                    data[i].text.title = e.target.value;
                                                    if (data[i].save === false) {
                                                        data[i].save = true
                                                    }
                                                    setPayload(data)
                                                }}
                                                error={false}
                                                name={'Title Slider ' + (Number(i) + 1)}
                                            />
                                        </div>
                                        <Space />
                                        {
                                            row.text.content ?
                                                row.text.content.map((cRow, cI) => (
                                                    (
                                                        <div key={cI} className={'flex-row-left'} style={{
                                                            margin: '10px 40px'
                                                        }}>
                                                            <div style={{
                                                                width: 'calc(40vw)'
                                                            }}>
                                                                <InputTextNormal
                                                                    small
                                                                    color={colors.light}
                                                                    value={cRow}
                                                                    onChange={(e) => {
                                                                        const data = [...payload];
                                                                        data[i].text.content[cI] = e.target.value;
                                                                        if (data[i].save === false) {
                                                                            data[i].save = true
                                                                        }
                                                                        setPayload(data)
                                                                    }}
                                                                    error={false}
                                                                    name={'Content ' + (Number(cI) + 1)}
                                                                />
                                                            </div>
                                                            {
                                                                Number(cI) === (row.text.content.length - 1) ? (
                                                                    <IconButton
                                                                        color="inherit"
                                                                        onClick={() => {
                                                                            let data = [...payload];
                                                                            data[i].text.content = [...data[i].text.content, 'new content']
                                                                            if (payload[i].text.content.length > 4) {
                                                                                alert('max content slider')
                                                                            } else {
                                                                                setPayload(data);
                                                                            }
                                                                        }}
                                                                        style={{
                                                                            margin: '0 50px'
                                                                        }}
                                                                    >
                                                                        <AddCircleIcon style={{
                                                                            color: colors.light,
                                                                            fontSize: 40
                                                                        }} />
                                                                    </IconButton>
                                                                ) : (
                                                                        null
                                                                    )
                                                            }
                                                        </div>
                                                    )
                                                )) : null}
                                    </Grid>
                                    <Grid item xs={4} style={{
                                        padding: 20
                                    }}>
                                        <div onClick={() => {
                                            action({
                                                type: 'setDopen',
                                                name: 'slider' + i
                                            })
                                        }}>
                                            {
                                                state.name === 'slider' + i && state.image !== '' ?
                                                    (
                                                        <img
                                                            style={{
                                                                width: '100%',
                                                                height: 400,
                                                                margin: 0,
                                                            }}
                                                            src={state.image}
                                                            alt={'props.image'}
                                                        />
                                                    ) :
                                                    row.img === null ? (
                                                        <div style={{
                                                            width: '100%',
                                                            height: 400,
                                                            backgroundColor: colors.fourdary,
                                                            cursor: 'pointer'
                                                        }} className={'flex-row-center'}>
                                                            <TextContent title color={colors.light}>No Image Found</TextContent>
                                                        </div>
                                                    ) : (
                                                            <img
                                                                style={{
                                                                    width: '100%',
                                                                    height: 400,
                                                                    margin: 0,
                                                                }}
                                                                src={row.img}
                                                                alt={'props.image'}
                                                            />
                                                        )
                                            }
                                        </div>
                                        <div
                                            onClick={() => {
                                                const data = [...payload];
                                                data[i].save = false
                                                setPayload(data)
                                                doStore(row)
                                            }}
                                            className={'button-add-slider flex-row-left'}>
                                            <SaveIcon style={{
                                                color: colors.light,
                                                padding: 10,
                                                marginLeft: 'auto',
                                                fontSize: 70
                                            }} />
                                            <div style={{
                                                marginRight: 'auto'
                                            }}>
                                                <TextTitle color={colors.light}>Save Slider</TextTitle>
                                            </div>
                                        </div>
                                    </Grid>
                                </Grid>
                            </div>
                            {
                                Number(i) !== (payload.length - 1) ? (
                                    <Space size={50} />
                                ) : (
                                        null
                                    )
                            }
                        </div>
                    ))
                }
                {
                    payload.length > 4 ? (
                        null
                    ) : (
                            <div
                                onClick={() => {
                                    addSlider()
                                }}
                                className={'button-add-slider flex-row-center'}>
                                <AddCircleIcon style={{
                                    color: colors.light,
                                    padding: 10,
                                    fontSize: 70
                                }} />
                            </div>
                        )
                }
            </Grid>
        </Grid>
    )
}

export const Menu = (props) => {

    const [state, action] = useTracked();
    const [payload, setPayload] = useState([]);
    const [master, setMaster] = useState(false);
    const [editor, setEditor] = useState(false)

    async function setData() {
        try {
            action({
                type: 'loadStart'
            })
            const request = await adminApi({
                method: 'get',
                url: 'landing-menu',
                data: {}
            })
            if (request) {
                if (!request.error) {
                    setPayload(request.data)
                    getMaster(request.data)
                } else {
                    action({
                        type: 'errorAlert',
                    })
                }
            }
        } catch (error) {
            console.log(error)
        }
    }

    async function getMaster(menu) {
        let data = await uccApi({
            method: "post",
            url: "masterAdminAccess",
            data: {}
        })
        if (data) {
            for (let i in data) {
                for (let j in menu) {
                    if (data[i].idmasterAdminAccess === menu[j].idmasterAdminAccess && !menu[j].hidden) {
                        data[i].checked = true
                    }
                }
            }
            setMaster(data)
            action({
                type: 'loadStop'
            })
        }
    }

    async function updateData(data, i) {
        try {
            const update = await adminApi({
                method: 'put',
                url: 'landing-menu',
                data: {
                    id: data.id,
                    img: 'menu' + i === state.name ? state.image === '' ? null : state.image : data.img,
                    title: data.title,
                    order: data.order,
                    hidden: data.hidden
                }
            })
            if (Number(i) === (payload.length - 1)) {
                if (update) {
                    action({
                        type: 'loadStop'
                    })
                    if (!update.error) {
                        action({
                            type: 'successAlert',
                        })
                    } else {
                        action({
                            type: 'errorAlert',
                        })
                    }
                }
            }
        } catch (error) {
            console.log(error)
        }
    }

    async function saveData(data, i) {
        try {
            const update = await adminApi({
                method: 'post',
                url: 'landing-menu',
                data: {
                    idmasterAdminAccess: data.idmasterAdminAccess,
                    img: 'menu' + i === state.name ? state.image === '' ? null : state.image : data.img,
                    title: data.title,
                    order: data.order,
                    hidden: false
                }
            })
            if (Number(i) === (payload.length - 1)) {
                if (update) {
                    action({
                        type: 'loadStop'
                    })
                    if (!update.error) {
                        action({
                            type: 'successAlert',
                        })
                    } else {
                        action({
                            type: 'errorAlert',
                        })
                    }
                }
            }
        } catch (error) {
            console.log(error)
        }
    }

    function doStore() {
        action({
            type: 'loadStart'
        })
        for (let i in payload) {
            const data = payload[i];
            if (data.id) {
                updateData(data, i)
            } else {
                saveData(data, i)
            }
        }
    }

    function openEditor(row, i) {
        setEditor(i)
    }


    useEffect(() => {
        setData()
    }, [])

    return (
        <Grid container
            direction="row">
            <Grid item md={12} style={{
                backgroundColor: colors.greyL,
                margin: 10,
                padding: 20
            }}>
                <div style={{
                    padding: 20
                }}>
                    <TextTitle>Master Menu</TextTitle>
                </div>
                <div style={{
                    flexWrap: 'wrap'
                }} className={'flex-row-left'}>
                    {
                        master ?
                            master.map((row, i) => (
                                <div style={{
                                    display: 'flex',
                                    margin: '20px 10px',
                                    position: 'relative'
                                }} key={i}
                                    onClick={() => {
                                        if (row.idmasterAdminAccess === 10) {
                                            alert('Super Admin Cannot added to public menu')
                                        } else {
                                            const spy = payload.filter(item => item.idmasterAdminAccess === row.idmasterAdminAccess)
                                            if (spy.length === 0 || spy[0].hidden) {
                                                if (spy.length === 0) {
                                                    const data = [...payload, {
                                                        id: false,
                                                        idmasterAdminAccess: row.idmasterAdminAccess,
                                                        img: null,
                                                        title: row.AdminAccessName,
                                                        order: (payload.length)
                                                    }];
                                                    setPayload(data)
                                                    const update = [...master];
                                                    update[i].checked = true;
                                                    setMaster(update)
                                                } else {
                                                    const field = payload.filter(item => item.idmasterAdminAccess === row.idmasterAdminAccess)[0];
                                                    const index = payload.indexOf(field);
                                                    const newData = [...payload];
                                                    newData[index].hidden = false;
                                                    setPayload(newData)
                                                    const update = [...master];
                                                    update[i].checked = true;
                                                    setMaster(update)
                                                }
                                            } else {
                                                    const field = payload.filter(item => item.idmasterAdminAccess === row.idmasterAdminAccess)[0];
                                                    const index = payload.indexOf(field);
                                                    const newData = [...payload];
                                                    newData[index].hidden = true;
                                                    setPayload(newData)
                                                    const update = [...master];
                                                    update[i].checked = false;
                                                    setMaster(update)
                                            }
                                        }
                                    }}
                                >
                                    <LeaderCard
                                        active={row.checked}
                                        size={70}
                                        image={
                                            null
                                        }
                                        title={row.AdminAccessName}
                                    />
                                    {
                                        row.checked ? (
                                            <div style={{
                                                position: 'absolute',
                                                top: 0,
                                                left: 0,
                                            }}>
                                                <CheckCircleIcon style={{
                                                    fontSize: 40,
                                                    color: colors.danger
                                                }} />
                                            </div>
                                        ) : (
                                                null
                                            )
                                    }
                                </div>
                            )) : null
                    }
                </div>
                <Space size={30} />
            </Grid>
            <Grid item md={12} className={'flex-column-center'} style={{
                backgroundColor: colors.greyL,
                margin: 10,
                width: '100%',
                padding: 20
            }}>
                <div style={{
                    padding: 20,
                    alignSelf: 'flex-start'
                }}>
                    <TextTitle>Active Page Menu</TextTitle>
                </div>
                <Space size={50} />
                <div style={{
                    flexWrap: 'wrap'
                }} className={'flex-row-left'}>
                    {payload.map((row, i) => (
                        row.hidden ? (null) : (
                            <div key={i} className={'flex-column-center'}>
                                <div style={{
                                    display: 'flex',
                                    margin: 10,
                                }} onClick={() => {
                                    openEditor(row, i)
                                }}>
                                    <LeaderCard
                                        size={80}
                                        image={row.img}
                                        title={row.title}
                                    />
                                </div>
                                <div style={{
                                    padding: 10,
                                    width: 'calc(100% - 20px)',
                                    backgroundColor: colors.greyD,
                                    marginTop: 5
                                }} className={'flex-row-space'}>
                                    {
                                        i === 0 ? (
                                            <div />
                                        ) : (
                                                <div
                                                    className={'flex-row-center button-small-page'}
                                                    onClick={() => {
                                                        let data = [...payload];
                                                        data[i].order = i - 1;
                                                        data[i - 1].order = i + 1;
                                                        let move = mover(data, i, (i - 1));
                                                        console.log(move)
                                                        setPayload(move);
                                                    }}
                                                >
                                                    <ChevronLeftIcon />
                                                </div>
                                            )
                                    }
                                    <TextContent color={colors.light}>Page {Number(i) + 1}</TextContent>
                                    {
                                        Number(i) === (Number(payload.length) - 1) ? (
                                            <div />
                                        ) : (
                                                <div
                                                    onClick={() => {
                                                        let data = [...payload];
                                                        data[i].order = i + 1;
                                                        data[i + 1].order = i - 1;
                                                        let move = mover(data, i, (i + 1));
                                                        console.log(move)
                                                        setPayload(move);
                                                    }}
                                                    className={'flex-row-center button-small-page'}>
                                                    <ChevronRightIcon />
                                                </div>
                                            )
                                    }
                                </div>
                                {
                                    editor !== false && editor === i ? (
                                        <div className={'editor'}>
                                            <div style={{
                                                pading: 20
                                            }}>
                                                <TextTitle color={colors.light} center>Update Menu</TextTitle>
                                            </div>
                                            <InputTextNormal
                                                value={payload[editor].title}
                                                onChange={null}
                                                color={colors.light}
                                                disabled
                                                error={false}
                                                name={'title ' + Number(i + 1)}
                                            />
                                            <div style={{
                                                padding: 10
                                            }}>
                                                <TextContent color={colors.light}>Image</TextContent>
                                            </div>
                                            <div
                                                onClick={() => {
                                                    action({
                                                        type: 'setDopen',
                                                        name: 'menu' + i
                                                    })
                                                }} className={'flex-row-center'}>
                                                {state.name === 'menu' + i && state.image !== '' ? (
                                                    <img
                                                        style={{
                                                            width: 100,
                                                            height: 100,
                                                            margin: 0
                                                        }}
                                                        src={state.image}
                                                        alt={'props.image'}
                                                    />
                                                ) : (
                                                        row.img === null ? (
                                                            <div style={{
                                                                width: 100,
                                                                height: 100,
                                                                backgroundColor: colors.fourdary,
                                                                cursor: 'pointer'
                                                            }} className={'flex-row-center'}>
                                                                <TextContent title color={colors.light}>
                                                                    No Image Found
                                                    </TextContent>
                                                            </div>
                                                        ) : (
                                                                <img
                                                                    style={{
                                                                        width: 100,
                                                                        height: 100,
                                                                        margin: 0
                                                                    }}
                                                                    src={row.img}
                                                                    alt={'props.image'}
                                                                />
                                                            )
                                                    )}
                                            </div>
                                            <div className={'flex-row-center'} style={{
                                                margin: '40px 0 0 0'
                                            }}>
                                                <Button
                                                    onClick={() => {
                                                        setEditor(false)
                                                    }}
                                                    variant="contained" size={'small'} color="primary">
                                                    cancel
                                        </Button>
                                                <div style={{
                                                    width: 20
                                                }} />
                                                <Button
                                                    onClick={() => {
                                                        let data = [...payload];
                                                        data[i].img = state.image
                                                        setPayload(data);
                                                        setEditor(false)
                                                    }}
                                                    variant="contained" size={'small'} color="primary">
                                                    Save
                                        </Button>
                                            </div>
                                        </div>
                                    ) : (
                                            null
                                        )
                                }
                            </div>
                        )
                    ))}
                </div>
                <Space size={100} />
                <Button
                    onClick={() => {
                        doStore()
                    }}
                    style={{
                        width: 'calc(100% - 20vw)'
                    }} variant="contained" size={'large'} color="primary">
                    Save
                </Button>
                <Space size={30} />
            </Grid>
            <Space size={50} />
        </Grid>
    )
}

export const About = (props) => {

    const [state, action] = useTracked()
    const [payload, setPayload] = useState({
        img: null,
        content: ['']
    })

    async function doStore(data, i) {
        try {
            action({
                type: 'loadStart'
            })
            const update = await adminApi({
                method: 'put',
                url: 'landing-about',
                data: {
                    img: 'about' === state.name ? state.image === '' ? null : state.image : data.img,
                    content: JSON.stringify(payload.content)
                }
            })
            if (update) {
                action({
                    type: 'loadStop'
                })
                if (!update.error) {
                    action({
                        type: 'successAlert',
                    })
                    setPayload(update.data.payload)
                } else {
                    action({
                        type: 'errorAlert',
                    })
                }
            }
        } catch (error) {
            console.log(error)
        }
    }

    async function setData() {
        try {
            action({
                type: 'loadStart'
            })
            const request = await adminApi({
                method: 'get',
                url: 'landing-about',
                data: {}
            })
            if (request) {
                action({
                    type: 'loadStop'
                })
                if (!request.error) {
                    setPayload(request.data)
                } else {
                    action({
                        type: 'errorAlert',
                    })
                }
            }
        } catch (error) {
            console.log(error)
        }
    }


    useEffect(() => {
        setData()
    }, [])

    return (
        <Grid container
            direction="row">
            <Grid item md sm={12} className={'flex-column-center'} style={{
                backgroundColor: colors.greyL,
                margin: 10,
            }}>
                <div style={{
                    padding: '100px 20px'
                }}>
                    <AboutCompoment payload={payload} overide={{
                        img: state.image,
                        id: state.name
                    }} />
                </div>
            </Grid>
            <Grid item md sm={12} className={'flex-column-start'} style={{
                backgroundColor: colors.greyL,
                margin: 10,
                padding: 20,
                width: '100%'
            }}>
                {payload.content.map((row, i) => (
                    <div key={i} style={{
                        width: '100%'
                    }}>
                        <InputTextNormal
                            value={row}
                            onChange={(e) => {
                                const data = { ...payload };
                                data.content[i] = e.target.value;
                                setPayload(data)
                            }}
                            error={false}
                            name={'about content ' + (Number(i) + 1)}
                        />
                    </div>
                ))}
                <Space size={100} />
                <div className={'flex-row-center'} style={{
                    width: '100%'
                }}>
                    <div style={{
                        width: '30%',
                        marginRight: 20
                    }} onClick={() => {
                        action({
                            type: 'setDopen',
                            name: 'about'
                        })
                    }}>
                        <div style={{
                            cursor: 'pointer',
                            padding: 20,
                            backgroundColor: colors.secondary,
                            borderRadius: 7
                        }} className={'flex-row-center'}>
                            <PermMediaIcon style={{
                                color: colors.light
                            }} />
                            <div style={{
                                marginLeft: 10
                            }}>
                                <TextTitle center color={colors.light}>Change Image</TextTitle>
                            </div>
                        </div>
                    </div>
                    <Button
                        onClick={() => {
                            doStore()
                        }}
                        style={{
                            width: '100%'
                        }} variant="contained" size={'large'} color="primary">
                        <TextTitle color={colors.light}>Save</TextTitle>
                    </Button>
                </div>
            </Grid>
        </Grid>
    )
}


export const Feature = (props) => {

    const [state, action] = useTracked()
    const [payload, setPayload] = useState({
        img: null,
        left: [
            {
                title: '',
                content: '',
                icon: null
            }
        ],
        right: [
            {
                title: '',
                content: '',
                icon: null
            }
        ]
    })

    async function setData() {
        try {
            action({
                type: 'loadStart'
            })
            const request = await adminApi({
                method: 'get',
                url: 'landing-feature',
                data: {}
            })
            if (request) {
                action({
                    type: 'loadStop'
                })
                if (!request.error) {
                    setPayload(request.data)
                } else {
                    action({
                        type: 'errorAlert',
                    })
                }
            }
        } catch (error) {
            console.log(error)
        }
    }

    async function doStore(data, i) {
        try {
            action({
                type: 'loadStart'
            })
            const update = await adminApi({
                method: 'put',
                url: 'landing-feature',
                data: {
                    img: 'feature' === state.name ? state.image === '' ? null : state.image : payload.img,
                    left: JSON.stringify(payload.left),
                    right: JSON.stringify(payload.right)
                }
            })
            if (update) {
                action({
                    type: 'loadStop'
                })
                if (!update.error) {
                    action({
                        type: 'successAlert',
                    })
                    setPayload(update.data.payload)
                } else {
                    action({
                        type: 'errorAlert',
                    })
                }
            }
        } catch (error) {
            console.log(error)
        }
    }


    useEffect(() => {
        setData()
    }, [])

    return (
        <Grid container
            direction="row">
            <Grid item sm={12} className={'flex-column-center'} style={{
                backgroundColor: colors.greyL,
                margin: 10,
            }}>
                <div style={{
                    padding: 50
                }}>
                    <FeatureComponent payload={payload} overide={{
                        img: state.image,
                        id: state.name
                    }} />
                </div>
            </Grid>
            <Grid container
                direction="row"
                justify="center"
                alignItems="center">
                <Grid item md className={'flex-column-start'} style={{
                    backgroundColor: colors.greyL,
                    margin: 10,
                    padding: 20,
                    width: '100%'
                }}>
                    <TextTitle>Left Content</TextTitle>
                    <Space size={30} />
                    {payload.left.map((row, i) => (
                        <div key={i} style={{
                            width: '100%'
                        }}>
                            <InputTextNormal
                                value={row.title}
                                onChange={(e) => {
                                    const data = { ...payload };
                                    data.left[i].title = e.target.value;
                                    setPayload(data)
                                }}
                                error={false}
                                name={'title ' + (Number(i) + 1)}
                            />
                            <div style={{
                                marginLeft: 20
                            }}>
                                <InputTextNormal
                                    size={10}
                                    small
                                    value={row.content}
                                    onChange={(e) => {
                                        const data = { ...payload };
                                        data.left[i].content = e.target.value;
                                        setPayload(data)
                                    }}
                                    error={false}
                                    name={'sub-title ' + (Number(i) + 1)}
                                />
                            </div>
                        </div>
                    ))}
                    <Space size={100} />
                    <div className={'flex-row-center'} style={{
                        width: '100%'
                    }}>
                    </div>
                </Grid>
                <Grid item md className={'flex-column-start'} style={{
                    backgroundColor: colors.greyL,
                    margin: 10,
                    padding: 20,
                    width: '100%'
                }}>
                    <TextTitle>Right Content</TextTitle>
                    <Space size={30} />
                    {payload.right.map((row, i) => (
                        <div key={i} style={{
                            width: '100%'
                        }}>
                            <InputTextNormal
                                value={row.title}
                                onChange={(e) => {
                                    const data = { ...payload };
                                    data.right[i].title = e.target.value;
                                    setPayload(data)
                                }}
                                error={false}
                                name={'title ' + (Number(i) + 1)}
                            />
                            <div style={{
                                marginLeft: 20
                            }}>
                                <InputTextNormal
                                    size={10}
                                    small
                                    value={row.content}
                                    onChange={(e) => {
                                        const data = { ...payload };
                                        data.right[i].content = e.target.value;
                                        setPayload(data)
                                    }}
                                    error={false}
                                    name={'sub-title ' + (Number(i) + 1)}
                                />
                            </div>
                        </div>
                    ))}
                    <Space size={100} />
                    <div className={'flex-row-center'} style={{
                        width: '100%'
                    }}>
                    </div>
                </Grid>
            </Grid>
            <div style={{
                width: '100%',
                margin: 100
            }} className={'flex-row-center'}>
                <div style={{
                    width: '30%',
                    marginRight: 20
                }} onClick={() => {
                    action({
                        type: 'setDopen',
                        name: 'feature'
                    })
                }}>
                    <div style={{
                        cursor: 'pointer',
                        padding: 20,
                        backgroundColor: colors.secondary,
                        borderRadius: 7
                    }} className={'flex-row-center'}>
                        <PermMediaIcon style={{
                            color: colors.light
                        }} />
                        <div style={{
                            marginLeft: 10
                        }}>
                            <TextTitle center color={colors.light}>Change Image</TextTitle>
                        </div>
                    </div>
                </div>
                <Button style={{
                    width: '70%',
                }}
                    variant="contained"
                    size={'large'}
                    color="primary"
                    onClick={() => {
                        doStore()
                    }}
                >
                    <TextTitle color={colors.light}>Save</TextTitle>
                </Button>
            </div>
        </Grid>
    )
}

export default Slider

