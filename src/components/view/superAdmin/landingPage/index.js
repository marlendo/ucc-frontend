import ProfileComponent from './profile';
import SliderComponent from './slider';
import MenuComponent from './menu';
import AboutComponent from './about';
import FeatureComponent from './feature';

export const Profile = ProfileComponent;
export const Slider = SliderComponent;
export const Menu = MenuComponent;
export const About = AboutComponent;
export const Feature = FeatureComponent;