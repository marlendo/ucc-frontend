import React, { useEffect, useState } from "react";
import { Drawer } from '@material-ui/core'
import { useTracked } from '../../../service';
import Index from '..';
import colors from '../../../constants/color';
import { TextTitle, TextContent } from '../../../components/text';
import HomeRoundedIcon from '@material-ui/icons/HomeRounded';
import ChevronRightIcon from '@material-ui/icons/ChevronRight';
import { mainMenu } from '../../../constants';
import DeveloperBoardIcon from '@material-ui/icons/DeveloperBoard';

// page
import Dashboard from './dashboard';
import Landing from "./landing";
import { ImagePickerComponent } from "../../container";
import { getLandingMenu } from "../../../service/api";
import UccManagement from "./ucc";

export const SuperAdminPage = (props) => {

    const [state, action] = useTracked()
    const [main, setMain] = useState(false);

    async function initialMenu() {
        setMain(mainMenu)
        const data = await getLandingMenu()
        let newData = mainMenu;
        for (let i in data) {
            newData.push({
                id: data[i].idmasterAdminAccess,
                img: <DeveloperBoardIcon className={'img-menu-logo'} />,
                title: data[i].title,
                child: [
                    {
                        key: 'mLanding',
                        id: data[i].idmasterAdminAccess,
                        title: 'Landing Page ' + data[i].title
                    },
                    {
                        key: 'mMenu',
                        id: data[i].idmasterAdminAccess,
                        title: 'Menu ' + data[i].title
                    }
                ]
            })
        }
        setMain(newData)
        action({
            type: 'setMenu',
            menu: mainMenu[0].title
        })
    }

    useEffect(() => {
        initialMenu()
    }, [])

    return (
        main && main.length > 2 ? (
            <Index menu={main} link={'sa'}>
                <div className={'flex-row-left'} style={{
                    padding: 10
                }}>
                    <TextTitle size={16} color={colors.dark}>{state.menu}</TextTitle>
                    <div style={{
                        height: 30,
                        width: 2,
                        margin: '0 0 0 20px',
                        backgroundColor: colors.greyL
                    }} />
                    <ChevronRightIcon style={{
                        color: colors.grey,
                        margin: '0 20px 0 10px',
                    }} />
                    <TextContent color={colors.grey} size={18}>{state.subMenu.title}</TextContent>
                </div>
                {
                    state.menu === main[0].title ? (
                        <Dashboard />
                    ) : (
                            state.menu === main[1].title ? (
                                <Landing menu={main[1].child} />
                            ) : (
                                    // state.menu === main[2].title ? (
                                        <UccManagement menu={main[2].child} />
                                    // ) : (
                                    //         <Dashboard />
                                    //     )
                                )
                        )
                }
            </Index>
        ) : null
    )
}