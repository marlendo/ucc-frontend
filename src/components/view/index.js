import React, { useState, useEffect } from 'react';
import { useTracked } from '../../service';
import colors from '../../constants/color';
import clsx from 'clsx';
import { makeStyles, useTheme } from '@material-ui/core/styles';
import { UccLogo } from '../image';
import { Badge, IconButton, Collapse, Drawer, AppBar, Toolbar, CssBaseline, Divider } from '@material-ui/core';
import images from '../../constants/images';
import { SearchComponent } from '../form';
import { TextTitle, TextContent } from '../text';
import { logOut } from '../../helpers/store';

import MenuIcon from '@material-ui/icons/Menu';
import ChevronLeftIcon from '@material-ui/icons/ChevronLeft';
import ChevronRightIcon from '@material-ui/icons/ChevronRight';
import MailIcon from '@material-ui/icons/Mail';
import NotificationsIcon from '@material-ui/icons/Notifications';



const drawerWidth = 240;

const useStyles = makeStyles(theme => ({
    root: {
        display: 'flex',
    },
    appBar: {
        zIndex: theme.zIndex.drawer + 1,
        transition: theme.transitions.create(['width', 'margin'], {
            easing: theme.transitions.easing.sharp,
            duration: theme.transitions.duration.leavingScreen,
        }),
    },
    appBarShift: {
        marginLeft: drawerWidth,
        width: `calc(100% - ${drawerWidth}px)`,
        transition: theme.transitions.create(['width', 'margin'], {
            easing: theme.transitions.easing.sharp,
            duration: theme.transitions.duration.enteringScreen,
        }),
    },
    menuButton: {
        marginRight: 36,
    },
    hide: {
        display: 'none',
    },
    drawer: {
        width: drawerWidth,
        flexShrink: 0,
        whiteSpace: 'nowrap',
    },
    drawerOpen: {
        width: drawerWidth,
        transition: theme.transitions.create('width', {
            easing: theme.transitions.easing.sharp,
            duration: theme.transitions.duration.enteringScreen,
        }),
    },
    drawerClose: {
        transition: theme.transitions.create('width', {
            easing: theme.transitions.easing.sharp,
            duration: theme.transitions.duration.leavingScreen,
        }),
        overflowX: 'hidden',
        width: theme.spacing(7) + 1,
        [theme.breakpoints.up('sm')]: {
            width: theme.spacing(9) + 1,
        },
    },
    toolbar: {
        display: 'flex',
        alignItems: 'center',
        justifyContent: 'flex-end',
        padding: theme.spacing(0, 1),
        // necessary for content to be below app bar
        ...theme.mixins.toolbar,
    },
    content: {
        flexGrow: 1,
        padding: theme.spacing(1),
    },
    inputRoot: {
        color: 'inherit',
    },
    inputInput: {
        padding: theme.spacing(1, 1, 1, 0),
        // vertical padding + font size from searchIcon
        paddingLeft: `calc(1em + ${theme.spacing(4)}px)`,
        transition: theme.transitions.create('width'),
        width: '100%',
        [theme.breakpoints.up('sm')]: {
            width: '12ch',
            '&:focus': {
                width: '20ch',
            },
        },
    },
}));

export default function MiniDrawer(props) {
    const classes = useStyles();
    const theme = useTheme();
    const [open, setOpen] = useState(true);
    const [state, action] = useTracked();

    const handleDrawerOpen = () => {
        setOpen(true);
    };

    const handleDrawerClose = () => {
        setOpen(false);
    };

    function setMenu(row) {
        action({
            type: 'setSubMenu',
            subMenu: row.child.length > 0 ? row.child[0] : ''
        })
        action({
            type: 'setMenu',
            menu: row.title
        })
    }

    function setSubMenu(row) {
        action({
            type: 'setSubMenu',
            subMenu: row
        })
    }

    useEffect(()=> {
        console.log(props.seo)
    }, [])

    return (
        <div className={classes.root}>
            <CssBaseline />
            <AppBar
                position="fixed"
                className={clsx(classes.appBar, {
                    [classes.appBarShift]: open,
                })}
                style={{
                    backgroundColor: colors.fourdary,
                }}
            >
                <Toolbar style={{
                    boxShadow: '0px 0px 5px rgba(18, 23, 39, 0.5)'
                }}>
                    <IconButton
                        color="inherit"
                        aria-label="open drawer"
                        onClick={handleDrawerOpen}
                        edge="start"
                        className={clsx(classes.menuButton, {
                            [classes.hide]: open,
                        })}
                    >
                        <MenuIcon />
                    </IconButton>
                    <div style={{
                        width: '100%',
                        height: 60,
                    }} className={'flex-row-space'}>
                        <div>
                            <SearchComponent />
                        </div>
                        <div className={'flex-row-right'}>
                            <IconButton aria-label="show 11 new notifications" color="inherit">
                                <Badge badgeContent={1} color="secondary">
                                    <MailIcon style={{
                                        fontSize: 25
                                    }} />
                                </Badge>
                            </IconButton>
                            <IconButton aria-label="show 11 new notifications" color="inherit">
                                <Badge badgeContent={2} color="secondary">
                                    <NotificationsIcon style={{
                                        fontSize: 25
                                    }} />
                                </Badge>
                            </IconButton>
                            <div style={{
                                marginLeft: 10,
                                cursor: 'pointer'
                            }} onClick={() => {
                                logOut()
                            }}>
                                <div style={{
                                    backgroundColor: colors.grey,
                                    height: 40,
                                    width: 40,
                                    borderRadius: 100
                                }} />
                            </div>
                        </div>
                    </div>
                </Toolbar>
            </AppBar>
            <Drawer
                variant="permanent"
                className={clsx(classes.drawer, {
                    [classes.drawerOpen]: open,
                    [classes.drawerClose]: !open,
                })}
                classes={{
                    paper: clsx({
                        [classes.drawerOpen]: open,
                        [classes.drawerClose]: !open,
                    }),
                }}
            >
                <div style={{
                    backgroundColor: colors.fourdary,
                    height: '100%'
                }}>
                    <div className={classes.toolbar} style={{
                        backgroundColor: colors.fourdary,
                        boxShadow: 'none',
                    }}>
                        <div style={{
                            width: '100%',
                            marginLeft: 10,
                            backgroundColor: colors.light,
                            padding: '5px 10px',
                            borderRadius: 7,
                            margin: '20px 5px'
                        }} className={'flex-row-left'}>
                            <UccLogo
                                small
                                img={
                                    images.logo
                                }
                                size={120}
                            />
                        </div>
                        <IconButton
                            color="inherit" onClick={handleDrawerClose}>
                            {theme.direction === 'rtl' ? <ChevronRightIcon style={{
                                color: colors.light,
                                fontSize: 30
                            }} /> : <ChevronLeftIcon style={{
                                color: colors.light,
                                fontSize: 30
                            }} />}
                        </IconButton>
                    </div>
                    {open ? (
                        <div style={{
                            padding: 20
                        }}>
                            <TextContent title color={colors.light} size={14}>{props.seo ? props.seo.page + ' menu' : 'Super Admin Menu'}</TextContent>
                        </div>
                    ) : (
                            <div className={'flex-row-center'}>
                                <TextContent title size={30} color={colors.light}>...</TextContent>
                            </div>
                        )}
                    <Divider />
                    <div style={{
                        margin: 10
                    }}>
                        {props.menu.map((row, i) => (
                            <div key={i} style={{
                                margin: '5px 0'
                            }}>
                                <div
                                    style={{
                                        backgroundColor: row.title === state.menu ? 'rgba(255, 255, 255, 0.23)' : null
                                    }}
                                    className={
                                        open ? 'flex-row-left button-side' : 'flex-row-center button-side'}
                                    onClick={() => {
                                        setMenu(row)
                                    }}>
                                    {row.img}
                                    {open ? (
                                        <div style={{
                                            padding: 10
                                        }}>
                                            <TextContent size={14} color={colors.light}>
                                                {row.title}
                                            </TextContent>
                                        </div>
                                    ) : (
                                            null
                                        )}
                                </div>
                                {
                                    row.child.length > 0 ? (
                                        <Collapse in={row.title === state.menu}>
                                            {
                                                row.child.map((cRow, cI) => (
                                                    <div key={cI} style={{
                                                        margin: open ? '5px 0 5px 10px' : '0'
                                                    }}>
                                                        <div
                                                            style={{
                                                                backgroundColor: cRow.title === state.subMenu.title ? 'rgba(255, 255, 255, 0.3)' : null
                                                            }}
                                                            className={
                                                                open ? 'flex-row-left button-side' : 'flex-row-center button-side'}
                                                            onClick={() => {
                                                                setSubMenu(cRow)
                                                            }}>
                                                            {open ? (
                                                                <div style={{
                                                                    padding: 10
                                                                }}>
                                                                    <TextContent size={14} color={colors.light}>
                                                                        {cRow.title}
                                                                    </TextContent>
                                                                </div>
                                                            ) : (
                                                                    <div style={{
                                                                        padding: '0 20px'
                                                                    }}>
                                                                        <TextTitle size={14} color={colors.light}>
                                                                            {cRow.title.toString().split(' ')[cRow.title.toString().split(' ').length - 1].substring(0, 1)}
                                                                        </TextTitle>
                                                                    </div>
                                                                )}
                                                        </div>
                                                    </div>
                                                ))
                                            }
                                        </Collapse>
                                    ) : (
                                            null
                                        )
                                }
                            </div>
                        ))}
                    </div>
                </div>
            </Drawer>
            <main className={classes.content}>
                <div className={classes.toolbar} />
                {props.children}
            </main>
        </div>
    );
}
