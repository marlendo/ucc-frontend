import React from 'react';
import { Line } from 'react-chartjs-2';
import colors from '../constants/color';
import { TextTitle } from './text';

export const HeaderChart = (props) => {

    const data = {
        labels: props.payload.labels,
        datasets: [
            {
                label: '',
                fill: true,
                lineTension: 0,
                backgroundColor: 'rgba(250,250,250 ,0.5)',
                borderColor: colors.light,
                borderCapStyle: 'butt',
                borderDash: [],
                borderDashOffset: 0.0,
                borderJoinStyle: 'miter',
                pointBorderColor: colors.light,
                pointBackgroundColor: colors.light,
                pointBorderWidth: 1,
                pointHoverRadius: 5,
                pointHoverBackgroundColor: colors.primary,
                pointHoverBorderColor: colors.light,
                pointHoverBorderWidth: 2,
                pointRadius: 1,
                pointHitRadius: 10,
                data: props.payload.data
            }
        ]
    };

    return (
        <div>
            <div style={{
                backgroundColor: props.color ? props.color : colors.dark,
                padding: 10,
                borderRadius: 15
            }}>
                <div style={{
                    padding: '5px 0 10px 5px'
                }}>
                    <TextTitle color={colors.light}>{props.label ? props.label : 'Chart Data'}</TextTitle>
                </div>
                <Line data={data} options={{
                    scales: {
                        xAxes: [{
                            display: false //this will remove all the x-axis grid lines
                        }],
                        yAxes: [{
                            display: false //this will remove all the x-axis grid lines
                        }]
                    },
                    legend: {
                        display: false
                    },
                }} />
            </div>
        </div>
    )
}