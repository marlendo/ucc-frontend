import React from "react";
import images from '../constants/images';
import useMediaQuery from '@material-ui/core/useMediaQuery';
import { useTheme } from '@material-ui/core/styles';

export const ImageBackground = (props) => {

  const theme = useTheme();
  const hide = useMediaQuery(theme.breakpoints.down('sm'));

  return (
    hide ? (
      <img style={{

      }} src={images.graduationsBg} alt={'graduations'} />
    ) : (
        <img className={'img-paralax-dashboard'} src={images.graduationsBg} alt={'graduations'} />
      )
  );
}

export const UccLogo = (props) => {

  return (
    <img
      src={props.img}
      style={{
        margin: props.small? 0 : 20,
        width: props.size ? props.size : 20
      }}
      alt={'logo ucc'} />
  )
}