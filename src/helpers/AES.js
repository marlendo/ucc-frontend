import CryptoJS from 'crypto-js'
import { getKey } from '../service/api';

export const encripter = function (object, key) {
    try {
        let ciphertext = CryptoJS.AES.encrypt(JSON.stringify(object), key);        
        return ciphertext.toString();
    } catch (error) {
        console.log(error)
        return 'password encription error'
    }
}

export const decripter = function (string, key) {
    try {
        var bytes = CryptoJS.AES.decrypt(string, key);        
        var plaintext = bytes.toString(CryptoJS.enc.Utf8);        
        return JSON.parse(plaintext);
    } catch (error) {
        return 'password encription error'
    }
}

export const generateToken = async function (payload){
    const keyData = await getKey();
    const key = keyData.data.values;    
    const token = encripter(payload, key);
    return {
        token,
        key
    };
}
