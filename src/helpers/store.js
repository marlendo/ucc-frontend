import CryptoJS from 'crypto-js';
import SecureStorage from 'secure-web-storage';

const SECRET_KEY = 'erickganteng123';

const store = typeof window !== `undefined` ? window.localStorage : null

const secureStorage = new SecureStorage(store, {
    hash: function hash(key) {
        key = CryptoJS.SHA256(key, SECRET_KEY);

        return key.toString();
    },
    encrypt: function encrypt(data) {
        data = CryptoJS.AES.encrypt(data, SECRET_KEY);

        data = data.toString();

        return data;
    },
    decrypt: function decrypt(data) {
        data = CryptoJS.AES.decrypt(data, SECRET_KEY);

        data = data.toString(CryptoJS.enc.Utf8);

        return data;
    }
});

export const getStorage = (key) => {
    try {
        if (typeof window !== `undefined`) {
            return secureStorage.getItem(key)
        }
    } catch (error) {
        console.log(error)
        return false
    }
}

export const setStorage = (key, value) => {
    try {
        if (typeof window !== `undefined`) {
            secureStorage.setItem(key, value)
        }
    } catch (error) {
        console.log(error)
    }
}

export const logOut = (key, value) => {
    try {
        if (typeof window !== `undefined`) {
            window.localStorage.clear();
            // window.location.reload();
            window.history.back();
        }
    } catch (error) {
        console.log(error)
    }
}