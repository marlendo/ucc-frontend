export const parser = (state) => {
    try {
        return JSON.parse(JSON.stringify(state))
    } catch (error) {
        console.log(error)
        return false
    }
}

export const mover = (array, oldIndex, newIndex) => {
    if (newIndex >= array.length) {
        var k = newIndex - array.length + 1;
        while (k--) {
            array.push(undefined);
        }
    }
    array.splice(newIndex, 0, array.splice(oldIndex, 1)[0]);
    return array;
}

export const chartYears = (data) => {

    const labels = ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'Agust', 'September', 'Oktober', 'November', 'December'];
    let newData = []

    for (let i in labels) {
        if (data[i]) {
            newData.push(
                data[i]
            )
        } else {
            newData.push(
                0
            )
        }
    }

    return {
        labels,
        data: newData
    }
}