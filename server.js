require('dotenv').config();
const express = require('express');
const bodyParser = require("body-parser");
const app = express();
const cors = require('cors')
const port = process.env.PORT || 8080;
const routes = require("./server/routes");
const serveIndex = require('serve-index')

var whitelist = ['http://localhost:8000', 'http://ucc-edu.com']
var corsOptions = {
  origin: function (origin, callback) {
    if (whitelist.indexOf(origin) !== -1) {
      callback(null, true)
    } else {
      callback(new Error('Not allowed by CORS'))
    }
  }
}

app.use(cors())
// app.use(cors(corsOptions))

function middleware404(req, res, next) {
  res.sendView = function(view) {
      return res.sendFile(__dirname + "/public/404/");
  }
  next();
}

app.use('/resources', express.static(__dirname + '/images'), serveIndex('images', { 'icons': true }));
app.use(express.static('public/'));

app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());
routes(app);

app.use(middleware404);

app.use((req, res) => {
  res.sendView('index.html');
});

app.listen(port, function() {
  console.log('Booting Application UCC ADMIN node js ... || using port ' + port);
});