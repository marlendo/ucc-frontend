module.exports = {
  siteMetadata: {
    title: `Universal Campus Consorsium`,
    description: `UCC EDU, UCC, UCC App, Persatuan Kampus Seluruh Indonesia, Universal Campus Consorsium adalah penyatu semua universitas di seluruh indonesia bahkan dunia`,
    author: `erikmarlendo@gmail.com`,
  },
  plugins: [
    `gatsby-plugin-react-helmet`,
    `gatsby-plugin-remove-trailing-slashes`,
    {
      resolve: `gatsby-source-filesystem`,
      options: {
        name: `images`,
        path: `${__dirname}/src/images`,
      },
    },
    `gatsby-transformer-sharp`,
    `gatsby-plugin-sharp`,
    {
      resolve: `gatsby-plugin-manifest`,
      options: {
        name: `ucc-web-management`,
        short_name: `ucc-web`,
        start_url: `/`,
        background_color: `#663399`,
        theme_color: `#663399`,
        display: `standalone`,
        icon: `src/images/gatsby-icon.png`, // This path is relative to the root of the site.
      },
    },
    'gatsby-plugin-offline',
    {
      resolve: 'gatsby-plugin-express',
      options: {
        output: 'config/gatsby-express.json',
      }
    }    
  ],
}
